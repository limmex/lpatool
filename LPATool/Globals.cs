﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace LPATool
{
    class Globals
    {
        public const int indexCfg      = 2;      // index of config file name
        public const int indexError    = 6;      // index of error code
        public const int indexWatchID  = 7;      // index of watch ID
        public const int indexHW       = 8;      // index of HW name
        public const int indexVersion  = 3;      // index of Tester version
        public const int indexDate     = 4;      // index of date of measurement
        public const int indexStart    = 16;     // index of first parameter
        public const int indexStartGSM = 17;     // index of first GSM measurement
        public const int indexStopGSM  = 40;     // index of last GSM measurement
        public const int testerVersion = 303;    // above this version, use second set of masks
        public const int skipLast = 3;
        public const int indexDefault = 0;
        public const int histogramSize = 20;    // max number of points in histogram
        public const int histogramYticks = 6;   // number of tick on y-axis of histogram
        public const string imageFile  = "Blank.jpg";
        public const string imageFile1 = "Blank1.jpg";
        public const float histoX = 330;
        public const float histoY = 330;
        public const float graphX = 1024;
        public const float graphY = 400;
        public const float offsetX = 60.0f;
        public const float offsetY = 50.0f;
        public const float deltaX = 5.0f;
        public const int deltaYGrid = 2;
        public const double MAX = +1e6;
        public const double MIN = -1e6;
        public const double CPK = 1.33;
        public const string NullError = "0x00000000";
        public const string VbatEnd5ErrorS = "100000";
        public const long VbatEnd5ErrorL   = 0x100000;
        public const string VWatchIDErrorS  = "20000000000";
        public const long   VWatchIDErrorL = 0x20000000000;
        public const string NoRepairs = "!SAV";
        public const string Repairs   = "SAV";
        public enum ResultItems { count = 0, min, average, sigma, max, cpk, minLimit, maxLimit, delta, histogramValues };
        public List<string> ResultItemNames = new List<string>() {"Count","Minimum","Average", "Sigma","Maximum", "CPK" };
        public List<Brush> colors1 = new List<Brush>();
        public List<Brush> colors2 = new List<Brush>();
        public Dictionary<string, List<string>> Versions = new Dictionary<string, List<string>>();
        public List<limitParamMatch> LPMatch                = new List<limitParamMatch>();
        public Dictionary<string, List<string>> Cases       = new Dictionary<string, List<string>>();
        public Dictionary<string, List<string>> AudioCases  = new Dictionary<string, List<string>>();
        public Dictionary<string, List<int>> indexCases     = new Dictionary<string, List<int>>();
        public Dictionary<string, string> ExcludeCases      = new Dictionary<string, string>();
        /// <summary>List of columns to be excluded from m_dict</summary>
        public List<string> ExcludeList = new List<string>();
        /// <summary>List of entries in statistic table</summary>
        public List<string> StatList = new List<string>();
        /// <summary>List of comparison possibilities</summary>
        public List<string> CompList = new List<string>();
        /// <summary>Struct containing limit-parameter pairs for identifieing columns names (parameter) in the input data files</summary>
        public struct limitParamMatch
        {
            public string limit;
            public string param;
            public limitParamMatch(string li, string pa)
            {
                this.limit  = li;
                this.param  = pa;
            }
        };

        private static Globals instance;

        private Globals() 
        {
            LPMatch.Add(new limitParamMatch("[Battery]",        "Battery Telit / V"));
            LPMatch.Add(new limitParamMatch("[Battery2]",       "Battery End / V"));
            LPMatch.Add(new limitParamMatch("[Battery3]",       "Battery End+5'' / V"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 0] RX", "RX Level @ TCH 975"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 0] TX", "TX Power @ TCH 975"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 1] RX", "RX Level @ TCH 38"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 1] TX", "TX Power @ TCH 38"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 2] RX", "RX Level @ TCH 38_2"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 2] TX", "TX Power @ TCH 38_2"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 3] RX", "RX Level @ TCH 124"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 3] TX", "TX Power @ TCH 124"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 4] RX", "RX Level @ TCH 512"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 4] TX", "TX Power @ TCH 512"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 5] RX", "RX Level @ TCH 661"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 5] TX", "TX Power @ TCH 661"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 6] RX", "RX Level @ TCH 736"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 6] TX", "TX Power @ TCH 736"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 7] RX", "RX Level @ TCH 885"));
            LPMatch.Add(new limitParamMatch("[MeasSetup 7] TX", "TX Power @ TCH 885"));
            LPMatch.Add(new limitParamMatch("Power",            "Tone Level / dB"));
            LPMatch.Add(new limitParamMatch("k2 Tone",          "Tone k2 / %"));
            LPMatch.Add(new limitParamMatch("k3 Tone",          "Tone k3 / %"));
            LPMatch.Add(new limitParamMatch("k2 GSM",           "dGSM k2 / %"));
            LPMatch.Add(new limitParamMatch("k3 GSM",           "dGSM k3 / %"));
            LPMatch.Add(new limitParamMatch("Tonemin",          "Tone Duration / ms"));
            LPMatch.Add(new limitParamMatch("Duramax",          "dDuration / ms"));
            LPMatch.Add(new limitParamMatch("Noisemax",         "Noise Level / dB"));
            LPMatch.Add(new limitParamMatch("[GSMHum]",         "dHum Level / dB"));
            LPMatch.Add(new limitParamMatch("[TPA]",            "TPA Gain / dB"));
            LPMatch.Add(new limitParamMatch("[GPSLimit1]",      "CNRmax / dB"));
            LPMatch.Add(new limitParamMatch("[GPSLimit2]",      "CNRmin / dB"));
            LPMatch.Add(new limitParamMatch("[Alarm1]",         "dt Alarm Tones / ms"));
            LPMatch.Add(new limitParamMatch("[Alarm2]",         "Alarm Level Low / dB"));
            LPMatch.Add(new limitParamMatch("[Alarm3]",         "Alarm Level High / dB"));
            // entries for special test
            LPMatch.Add(new limitParamMatch("tmp1", "MSP FW Version"));
            LPMatch.Add(new limitParamMatch("tmp2", "Telit FW Version"));
            LPMatch.Add(new limitParamMatch("tmp3", "IMEI"));
            LPMatch.Add(new limitParamMatch("tmp4", "ICCID"));
            LPMatch.Add(new limitParamMatch("tmp5", "IMSI"));
            LPMatch.Add(new limitParamMatch("tmp6", "Monitor String"));

            Versions.Add("V1.8.1-EU", new List<string>(new string[] { "V1.8.1-EU-Limmex.cfg", "Params_V1.8-EU.csv" }));
            Versions.Add("V1.8-EU", new List<string>(new string[]   { "V1.8-EU-Limmex.cfg", "Params_V1.8-EU.csv" }));
            Versions.Add("V1.8-US", new List<string>(new string[]   { "V1.8-US-Labor.cfg", "Params_V1.8-US.csv" }));
            Versions.Add("V1.5-EU", new List<string>(new string[]   { "V1.5-EU-Limmex.cfg", "Params_V1.5-EU.csv" }));
            Versions.Add("V1.5-US", new List<string>(new string[]   { "V1.5-US-Limmex.cfg",   "Params_V1.5-US.csv" }));
            Versions.Add("V1.1-EU", new List<string>(new string[]   { "V1.1-EU-Limmex.cfg",   "Params_V1.1-EU.csv" }));
            
            // first two masks for tester version < testerVersion
            // order must match with indices used inForm1.getStatVals()!
            Cases.Add("All samples",           new List<string>(new string[] { "",  NullError,"",  NullError}));
            Cases.Add("Only PASS",             new List<string>(new string[] { "0", NullError,"0", NullError}));
            Cases.Add("Any FAILS",             new List<string>(new string[] { "!0",NullError,"!0",NullError}));
            Cases.Add("Only ID fails",         new List<string>(new string[] { "0x0000000007E",        NullError,"0x000000007E",         NullError}));      // test order: ID, BattStart, GSM,Audio, GPS, battEnd, Alarm
            Cases.Add("Only BattStart fails",  new List<string>(new string[] { "0x00000000081", "0x0000000007E", "0x00000000081", "0x0000000007E" }));  // mask out all previous fails
            Cases.Add("Only GSM fails",        new List<string>(new string[] { "0x00000003F00", "0x000000000FF", "0x007FC000000", "0x00003FFFFFF" }));
            Cases.Add("Only Audio fails",      new List<string>(new string[] { "0x00001FFC000", "0x00000003FFF", "0x0000007FF00", "0x000000000FF" }));
            Cases.Add("Only GPS fails",        new List<string>(new string[] { "0x000E0000000", "0x00001FFFFFF", "0x00003800000", "0x000007FFFFF" }));
            Cases.Add("Only BattEnd fails",    new List<string>(new string[] { "0x00006000000", "0x000E1FFFFFF", "0x00000080000", "0x0000007FFFF" }));     // remove batt end +5"
            Cases.Add("Only Alarm fails",      new List<string>(new string[] { "0x00018000000", "0x000E7FFFFFF", "0x00000600000", "0x000001FFFFF" }));
            Cases.Add("Only Reset/Web fails",  new List<string>(new string[] { "0x00000000000", "0x00FFFFFFFFF", "0x11000000000", "0x00FFFFFFFFF" }));      // bit 41 is not taken into account
            Cases.Add("Exclude",               new List<string>(new string[] { "0x00000000000", "0x00FFFFFFFFF", "0x20000000000", "0x1FFFFFFFFFF" }));      // bit 41 to be counted as pass

            AudioCases.Add("k1 Tone",   new List<string>(new string[] {"0x00004000", "0x00003FFF", "0x00000100", "0x000000FF" }));
            AudioCases.Add("k2 Tone",   new List<string>(new string[] {"0x00008000", "0x00007FFF", "0x00000200", "0x000001FF" }));
            AudioCases.Add("k3 Tone",   new List<string>(new string[] {"0x00010000", "0x0000FFFF", "0x00000400", "0x000003FF" }));
            AudioCases.Add("Tonemin",   new List<string>(new string[] {"0x00020000", "0x0001FFFF", "0x00000800", "0x000007FF" }));
            AudioCases.Add("k1 GSM",    new List<string>(new string[] {"0x00040000", "0x0003FFFF", "0x00001000", "0x00000FFF" }));
            AudioCases.Add("k2 GSM",    new List<string>(new string[] {"0x00080000", "0x0007FFFF", "0x00002000", "0x00001FFF" }));
            AudioCases.Add("k3 GSM",    new List<string>(new string[] {"0x00100000", "0x000FFFFF", "0x00004000", "0x00003FFF" }));
            AudioCases.Add("Duramax",   new List<string>(new string[] {"0x00200000", "0x001FFFFF", "0x00008000", "0x00007FFF" }));
            AudioCases.Add("Noisemax",  new List<string>(new string[] {"0x00400000", "0x003FFFFF", "0x00010000", "0x0000FFFF" }));
            AudioCases.Add("GSMHum",    new List<string>(new string[] {"0x00800000", "0x007FFFFF", "0x00020000", "0x0001FFFF" }));
            AudioCases.Add("TPA gain",  new List<string>(new string[] {"0x01000000", "0x00FFFFFF", "0x00040000", "0x0003FFFF" }));

            // index in dict for corresponding failure modes
            indexCases.Add("All samples", new List<int>(new int[] { -1 }));
            indexCases.Add("Only PASS", new List<int>(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 }));
            indexCases.Add("Any FAILS", new List<int>(new int[] {-1}));
            indexCases.Add("Only ID fails", new List<int>(new int[] {31,32,33,34,35,36}));     
            indexCases.Add("Only BattStart fails", new List<int>(new int[] {0}));
            indexCases.Add("Only GSM fails", new List<int>(new int[] {3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}));
            indexCases.Add("Only Audio fails", new List<int>(new int[] {19,20,21,22,23,24,25,26,27,28}));
            indexCases.Add("Only GPS fails", new List<int>(new int[] {29,30}));
            indexCases.Add("Only BattEnd fails", new List<int>(new int[] {1,2}));
            indexCases.Add("Only Alarm fails", new List<int>(new int[] {-1}));
            
            ExcludeList.Add("GPS-FW");
            ExcludeList.Add("#Satellites in view");
            ExcludeList.Add("RX Quality @ TCH 975");
            ExcludeList.Add("RX Quality @ TCH 38");
            ExcludeList.Add("RX Quality @ TCH 38");     // twice in list for m_params
            ExcludeList.Add("RX Quality @ TCH 38_2");
            ExcludeList.Add("RX Quality @ TCH 124");
            ExcludeList.Add("RX Quality @ TCH 512");
            ExcludeList.Add("RX Quality @ TCH 661");
            ExcludeList.Add("RX Quality @ TCH 736");
            ExcludeList.Add("RX Quality @ TCH 885");
            ExcludeList.Add("dGSM Level / dB");
            ExcludeList.Add("dNoise Level / dB");
            ExcludeList.Add("Battery End+5'' / V");
            ExcludeList.Add("Remark 1");
            ExcludeList.Add("Remark 2");

            StatList.Add("Number of measurements:");
            StatList.Add("Number of pass:");
            StatList.Add("Number of pass with alarm:");
            StatList.Add("Number of fails:");
            StatList.Add("ID fails:");
            StatList.Add("GSM fails:");
            StatList.Add("Audio fails:");
            StatList.Add("GPS fails:");
            StatList.Add("Alarm fails:");
            StatList.Add("BattStart fails:");
            StatList.Add("BattEnd fails:");
            StatList.Add("Reset/WEB fails:");

            CompList.Add(">");
            CompList.Add(">=");
            CompList.Add("<");
            CompList.Add("<=");
            CompList.Add("==");
            CompList.Add("!=");
            // TX colors
            colors1.Add(Brushes.Blue);
            colors1.Add(Brushes.MediumAquamarine);
            colors1.Add(Brushes.PowderBlue);
            colors1.Add(Brushes.Aqua);
            colors1.Add(Brushes.Aquamarine);
            colors1.Add(Brushes.Azure);
            colors1.Add(Brushes.BlueViolet);
            colors1.Add(Brushes.CadetBlue);
            colors1.Add(Brushes.CornflowerBlue);
            colors1.Add(Brushes.SteelBlue);
            colors1.Add(Brushes.Violet);
            colors1.Add(Brushes.SlateBlue);
            colors1.Add(Brushes.SkyBlue);
            colors1.Add(Brushes.RoyalBlue);
            colors1.Add(Brushes.MidnightBlue);
            colors1.Add(Brushes.MediumSlateBlue);
            // RX colors
            colors2.Add(Brushes.Beige);
            colors2.Add(Brushes.Brown);
            colors2.Add(Brushes.RosyBrown);
            colors2.Add(Brushes.SaddleBrown);
            colors2.Add(Brushes.SandyBrown);
            colors2.Add(Brushes.Sienna);
            colors2.Add(Brushes.Tan);
            colors2.Add(Brushes.DarkOrange);
            colors2.Add(Brushes.MediumPurple);
            colors2.Add(Brushes.Purple);
            colors2.Add(Brushes.MediumOrchid);
            colors2.Add(Brushes.Maroon);
            colors2.Add(Brushes.Magenta);
            colors2.Add(Brushes.LightSalmon);
            colors2.Add(Brushes.LightPink);
            colors2.Add(Brushes.LightGoldenrodYellow);
            colors2.Add(Brushes.LightCoral);
            colors2.Add(Brushes.MediumTurquoise);
            colors2.Add(Brushes.MediumVioletRed);
        }

        public static Globals Instance
        {
            get 
            {
                if (instance == null)
                    instance = new Globals();
                return instance;
            }
        }
    }
}
