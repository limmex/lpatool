﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace LPATool
{
    public partial class SelectData : Form
    {
        private DateTime DB_START_DATE = new DateTime(2016, 1, 1);
        private const char DB_DATA_SPLITTER = '\r';

//        private string m_setupFileName = @"..\..\Setup.xml";
//        private string m_setupFileName = ApplicationDeployment.CurrentDeployment.ActivationUri.ToString() + @"\Setup.xml";
        private string m_setupFileName = "";

        public SelectData(Form1 caller)
        {
            m_caller = caller;
            InitializeComponent();
            m_dtpStart.Value = DB_START_DATE;
            m_dtpStop.Value = DateTime.Today;

            m_setupFileName = System.Windows.Forms.Application.StartupPath + @"\Setup.xml";
//            MessageBox.Show("Filename = " + m_setupFileName);
//            Setup se1 = new Setup("limmex-manufacturing.cylgcjpmtzkw.eu-west-1.rds.amazonaws.com", "results_generic", "dbuser", "#%I\\hu46cCvNvfAUrbVh", new List<string>(new string[] { "DT-Prod-001", "DT-Prod-002", "DT-Prod-003", "ALL" }));
//            Setup.WriteConfigListToXmlFile(se1, "Setup.xml");
            loadSetup();
            m_cbDB_CheckedChanged(this,EventArgs.Empty);
        }
        // caller window
        private Form1 m_caller;
        // setup file for DB
        private Setup m_setup;
        public bool loadSetup()
        {
            bool flag = true;
            try
            {
                m_setup = Setup.ReadConfigListFromXmlFile(m_setupFileName);
                m_cbTesters.Items.Clear();
                foreach (string name in m_setup.TesterNames)
                {
                    m_cbTesters.Items.Add(name);
                }
                // select last entry
                m_cbTesters.SelectedIndex = m_cbTesters.Items.Count - 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading setup file for DB: \n" + ex.ToString(),"ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
                Debug.WriteLine("Error ooading setup file for DB: " + ex.ToString());
                flag = false;
            }
            return flag;
        }
        /// <summary>Selects the files according to the selected source</summary>
        private void m_btnFiles_Click(object sender, EventArgs e)
        {
            bool flag = true;
            if (m_cbDB.Checked)
            {
                ReadDB rdb = new ReadDB(m_setup);
                if(rdb.pingDB())
                {
                    List<string> rawData = rdb.readData(m_dtpStart.Value, m_dtpStop.Value, m_cbTesters.Text);
                    if (rawData.Count == 0)
                    {
                        MessageBox.Show("No data found in DB!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        flag = false;
                    }
                    else
                    {
                        // remove header part in data
                        List<string> dataSets = new List<string>();
                        string[] header;
                        string[] data;
                        string[] empty;
                        foreach (string measurement in rawData)
                        {
                            header = measurement.Split(DB_DATA_SPLITTER)[0].Split(';');
                            data = measurement.Split(DB_DATA_SPLITTER)[1].Split(';');
                            int diff = header.Length - data.Length;
                            if (diff > 0)
                            {
                                empty = new string[diff];
                                for (int i = 0; i < diff; i++)
                                {
                                    empty[i] = "";
                                }
                                data = data.Concat(empty).ToArray();
                            }
                            string value = "";
                            for (int i = 0; i < data.Length; i++)
                            {
                                value += data[i] + ";";
                            }
                            value.TrimEnd(';');
                            dataSets.Add(value);
                        }
                        m_caller.storeDataSets(dataSets.ToList());
                        m_caller.DataFromDataBase = true;
                        m_caller.loadData(true);
                        m_caller.enableButtons(true);
                    }
                }
                else 
                {
                    MessageBox.Show("Cannot open Database!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    flag = false;
                }
            }
            else
            {
                if (m_openFileD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string[] dataFiles = m_openFileD.FileNames;
                    if (dataFiles.Count() > 0)
                    {
                        m_caller.storeDataFiles(dataFiles);
                        m_caller.DataFromDataBase = false;
                        m_caller.loadData(true);
                        m_caller.enableButtons(true);
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            m_caller.enableButtons(flag);
            if(flag) this.Close();
        }

        private void m_cbDB_CheckedChanged(object sender, EventArgs e)
        {
            m_lblDB.Text   = "DB name   = ";
            m_lblHost.Text = "Host name = ";
            if (m_cbDB.Checked && m_setup != null)
            {
                m_lblDB.Text   += formatLength(m_setup.DatabaseName);
                m_lblHost.Text += formatLength(m_setup.HostName);
            }
            else
            {
                m_lblDB.Text += "?";
                m_lblHost.Text += "?";
            }
        }
        private string formatLength(string input)
        {
            if (input.Length > 25)
            {
                return input.Substring(0, 24)+"...";
            }
            else
            {
                return input;
            }
        }
        private void m_btnSetupFile_Click(object sender, EventArgs e)
        {
            if (m_openFileD4.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                m_setupFileName = m_openFileD4.FileName;
                loadSetup();
                m_cbDB_CheckedChanged(this, EventArgs.Empty);
            }                                                      

        }
    }
}
