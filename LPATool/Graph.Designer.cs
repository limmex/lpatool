﻿namespace LPATool
{
    partial class Graph
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_pbGraph = new System.Windows.Forms.PictureBox();
            this.m_saveFileImage = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addTitleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbGraph)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_pbGraph
            // 
            this.m_pbGraph.BackColor = System.Drawing.SystemColors.Window;
            this.m_pbGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbGraph.Location = new System.Drawing.Point(12, 33);
            this.m_pbGraph.Name = "m_pbGraph";
            this.m_pbGraph.Size = new System.Drawing.Size(1024, 400);
            this.m_pbGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.m_pbGraph.TabIndex = 9;
            this.m_pbGraph.TabStop = false;
            this.m_pbGraph.Click += new System.EventHandler(this.m_pbGraph_Click);
            // 
            // m_saveFileImage
            // 
            this.m_saveFileImage.Filter = "Image|*.jpg";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTitleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1044, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addTitleToolStripMenuItem
            // 
            this.addTitleToolStripMenuItem.Name = "addTitleToolStripMenuItem";
            this.addTitleToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.addTitleToolStripMenuItem.Text = "Add title";
            this.addTitleToolStripMenuItem.Click += new System.EventHandler(this.addTitleToolStripMenuItem_Click);
            // 
            // Graph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 442);
            this.Controls.Add(this.m_pbGraph);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1060, 480);
            this.MinimumSize = new System.Drawing.Size(1060, 480);
            this.Name = "Graph";
            this.Text = "Graph";
            ((System.ComponentModel.ISupportInitialize)(this.m_pbGraph)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox m_pbGraph;
        private System.Windows.Forms.SaveFileDialog m_saveFileImage;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addTitleToolStripMenuItem;
    }
}