﻿namespace LPATool
{
    partial class Data
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_btnClose = new System.Windows.Forms.Button();
            this.m_dgvData = new System.Windows.Forms.DataGridView();
            this.m_cbSelection = new System.Windows.Forms.ComboBox();
            this.m_lblSel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // m_btnClose
            // 
            this.m_btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_btnClose.Location = new System.Drawing.Point(344, 628);
            this.m_btnClose.Name = "m_btnClose";
            this.m_btnClose.Size = new System.Drawing.Size(197, 22);
            this.m_btnClose.TabIndex = 1;
            this.m_btnClose.Text = "Close";
            this.m_btnClose.UseVisualStyleBackColor = true;
            this.m_btnClose.Click += new System.EventHandler(this.m_btnClose_Click);
            // 
            // m_dgvData
            // 
            this.m_dgvData.AllowUserToDeleteRows = false;
            this.m_dgvData.AllowUserToResizeColumns = false;
            this.m_dgvData.AllowUserToResizeRows = false;
            this.m_dgvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.m_dgvData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.m_dgvData.BackgroundColor = System.Drawing.SystemColors.Control;
            this.m_dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvData.Location = new System.Drawing.Point(14, 12);
            this.m_dgvData.Name = "m_dgvData";
            this.m_dgvData.ReadOnly = true;
            this.m_dgvData.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_dgvData.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.m_dgvData.Size = new System.Drawing.Size(527, 610);
            this.m_dgvData.TabIndex = 2;
            this.m_dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgv_CellContentClick);
            // 
            // m_cbSelection
            // 
            this.m_cbSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_cbSelection.FormattingEnabled = true;
            this.m_cbSelection.Location = new System.Drawing.Point(119, 628);
            this.m_cbSelection.Name = "m_cbSelection";
            this.m_cbSelection.Size = new System.Drawing.Size(219, 21);
            this.m_cbSelection.TabIndex = 3;
            this.m_cbSelection.Visible = false;
            this.m_cbSelection.SelectedIndexChanged += new System.EventHandler(this.m_cbSelection_SelectedIndexChanged);
            // 
            // m_lblSel
            // 
            this.m_lblSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_lblSel.AutoSize = true;
            this.m_lblSel.Location = new System.Drawing.Point(12, 632);
            this.m_lblSel.Name = "m_lblSel";
            this.m_lblSel.Size = new System.Drawing.Size(101, 13);
            this.m_lblSel.TabIndex = 4;
            this.m_lblSel.Text = "Selected key figure:";
            this.m_lblSel.Visible = false;
            // 
            // Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 662);
            this.Controls.Add(this.m_lblSel);
            this.Controls.Add(this.m_cbSelection);
            this.Controls.Add(this.m_dgvData);
            this.Controls.Add(this.m_btnClose);
            this.Name = "Data";
            this.Text = "Data";
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_btnClose;
        private System.Windows.Forms.DataGridView m_dgvData;
        private System.Windows.Forms.ComboBox m_cbSelection;
        private System.Windows.Forms.Label m_lblSel;
    }
}