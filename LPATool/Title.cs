﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LPATool
{
    public partial class Title : Form
    {
        private string m_title = "Title";
        public string GetTitle
        {
            get { return m_title; }
        }
        public Title()
        {
            InitializeComponent();
            m_tbTitle.Text = m_title;
        }

        private void m_btnOK_Click(object sender, EventArgs e)
        {
            m_title = m_tbTitle.Text;
            this.Close();
        }

        private void m_btnCn_Click(object sender, EventArgs e)
        {
            m_title = "";
            this.Close();
        }
    }
}
