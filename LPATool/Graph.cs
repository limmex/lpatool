﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace LPATool
{
    public partial class Graph : Form
    {
        private Graphics m_g;
        private Form m_f;
        private Random m_rnd = new Random();
        public Graph(Form f)
        {
            InitializeComponent();
            m_f = f;
        }

        private void m_pbGraph_Click(object sender, EventArgs e)
        {
            DialogResult aws = m_saveFileImage.ShowDialog();
            if ((m_saveFileImage.FileName != "") && aws == System.Windows.Forms.DialogResult.OK)
                m_pbGraph.Image.Save(m_saveFileImage.FileName);
        }
        /// <summary>Draws a list of floats and a title. X-axis = week numbers </summary>
        public void drawGraph(List<float> data, string id, List<int> weeks)
        {
            Bitmap myBitmap = new Bitmap(Globals.imageFile1);
            m_g = Graphics.FromImage(myBitmap);
            Font Bfont = new System.Drawing.Font("Consolas", 10, FontStyle.Bold);
            Font font = new System.Drawing.Font("Consolas", 8, FontStyle.Regular);
            // Set format of string
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.NoWrap;
            drawFormat.Alignment = StringAlignment.Far;
            // Determine min and max indice for GSM measurement in data (header, ie version dependent!)
            float min = data.Min();
            float max = data.Max();
            int nrPoints = data.Count;
            // Calc graph scaling in y and delta in x
            float scalingY = 1;
            if ((max - min) != 0)
            {
                scalingY = (Globals.graphY - 2 * Globals.offsetY) / (max - min);
            }
            float offsetZero = -(min * scalingY);
            float dx = (Globals.graphX - 2 * Globals.offsetX) / (nrPoints - 1);
            float dy = (Globals.graphY - 2 * Globals.offsetY) / ((int)(max - min));
            float offsetRx = dx * nrPoints;
            float x1, y1;//, x2, y2 = 0;
            Pen p = new Pen(Brushes.Blue);
            Debug.WriteLine("minY,maxY = " + min.ToString() + ',' + max.ToString());
            Debug.WriteLine("Scaling in y = " + scalingY.ToString());
            Debug.WriteLine("Zero offset in y = " + offsetZero.ToString());
            // y-grid
            m_g.DrawLine(Pens.Black, Globals.offsetX - 10f, Globals.graphY - Globals.offsetY + 2f, Globals.offsetX - 10f, Globals.offsetY - 2f);
            for (int i = 0; i < nrPoints; i++)
            {
                x1 = Globals.offsetX + dx * i + 3;
                m_g.DrawLine(Pens.LightGray, x1, Globals.graphY - Globals.offsetY + 4, x1, Globals.offsetY - 2);
                m_g.DrawString(weeks[i].ToString(), font, Brushes.Black, x1 + 10, Globals.graphY - Globals.offsetY + 20, drawFormat);
            }
            // x-grid
            m_g.DrawLine(Pens.Black, Globals.offsetX - 10, Globals.graphY - Globals.offsetY + 2, Globals.graphX - Globals.offsetX + 10, Globals.graphY - Globals.offsetY + 2);
            float idy = max - min;
            int noGrids = 5;
            float deltaYGrid = idy / noGrids;
            for (int i = 0; i <= noGrids; i++)
            {
                y1 = (deltaYGrid * i) * scalingY;
                m_g.DrawLine(Pens.LightGray, Globals.offsetX - 12, Globals.graphY - Globals.offsetY - y1, Globals.graphX - Globals.offsetX + 10, Globals.graphY - Globals.offsetY - y1);
                string ys = (min + deltaYGrid * i).ToString("F2");
                m_g.DrawString(ys, font, Brushes.Black, Globals.offsetX - 15, Globals.graphY - Globals.offsetY - y1 - 7, drawFormat);
            }
            // plot data
            drawPoints("lim", p, data.ToArray(), dx, offsetZero, scalingY);
            // add title
            m_g.FillRectangle(Brushes.White, Globals.offsetX, Globals.offsetY / 3, Globals.graphX - 2 * Globals.offsetX, Globals.offsetY / 2);
            m_g.DrawString(id, Bfont, Brushes.Black, Globals.offsetX, Globals.offsetY / 4);
            m_pbGraph.Image = myBitmap;
            m_pbGraph.Refresh();
        }
        /// <summary>Draws the GSM measurements of the given watch array</summary>
        public void drawGraph(List<string[]> watches, string[] m_header, Dictionary<string, List<double>> limits)
        {
            Bitmap myBitmap = new Bitmap(Globals.imageFile1);
            m_g = Graphics.FromImage(myBitmap);
            Font Bfont = new System.Drawing.Font("Consolas", 10, FontStyle.Bold);
            Font font = new System.Drawing.Font("Consolas", 8, FontStyle.Regular);
            // Set format of string
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.NoWrap;
            drawFormat.Alignment = StringAlignment.Far;
            // Determine min and max indice for GSM measurement in data (header, ie version dependent!)
            int iMin = 0;
            int iMax = 0;
            int i = 0;
            string[] header = m_header[0].Split(';');
            while (!header[i].Contains("TX"))
                i++;
            iMin = i;
            while (header[i].Contains("TX") || header[i].Contains("RX"))
                i++;
            iMax = i-1;   
            // Format string value to floats and search min/max in data
            List<float[]> dataTx  = new List<float[]>();
            List<float[]> dataRx  = new List<float[]>();
            List<List<float>> limitTx = new List<List<float>>();
            List<List<float>> limitRx = new List<List<float>>();
            List<float> limitTx1 = new List<float>();
            List<float> limitTx2 = new List<float>();
            List<float> limitRx1 = new List<float>();
            List<float> limitRx2 = new List<float>();
            int nrPoints = (iMax + 1 - iMin) / 3;
            float min = float.MaxValue;
            float max = float.MinValue;
            foreach (string[] values in watches)
            {
                List<float> valsTx = new List<float>();
                List<float> valsRx = new List<float>();
                float valTx = 0;
                float valRx = 0;
                for (i = iMin; i < iMax; i = i + 3)
                {
                    if ((values.Length > i) && float.TryParse(values[i], out valTx))
                    {
                        valsTx.Add(valTx);
                        if (valTx > max)
                            max = valTx;
                        if (valTx < min)
                            min = valTx;
                    }
                    else
                        break;
                    if ((values.Length > i) && float.TryParse(values[i+1], out valRx))
                    {
                        valsRx.Add(valRx);
                        if (valRx > max)
                            max = valRx;
                        if (valRx < min)
                            min = valRx;
                    }
                    else
                        break;
                    // break on last GSM value
                    if (i >= iMax)
                        break;
                }
                dataTx.Add(valsTx.ToArray());
                dataRx.Add(valsRx.ToArray());
            }
            // Fill limits
            for (i = 0; i < 2*nrPoints; i=i+2)
            {
                List<double> dt = limits[Globals.Instance.LPMatch[i + 4].limit];
                List<float> ft = new List<float>();
                foreach(double val in dt)
                    ft.Add((float)val);
                limitTx.Add(ft);
                List<double> dr = limits[Globals.Instance.LPMatch[i + 3].limit];
                List<float> fr = new List<float>();
                foreach (double val in dr)
                    fr.Add((float)val);
                limitRx.Add(fr);
            }
            // check min/max in limits
            foreach (List<float> pair in limitRx)
            {
                if (pair[0] < min)
                    min = pair[0];
                if (pair[1] > max)
                    max = pair[1];
            }
            foreach (List<float> pair in limitTx)
            {
                if (pair[0] < min)
                    min = pair[0];
                if (pair[1] > max)
                    max = pair[1];
            }
            // sort upper and lower limits
            foreach (List<float> pair in limitTx)
            {
                limitTx1.Add(pair[0]);
                limitTx2.Add(pair[1]);
            }
            foreach (List<float> pair in limitRx)
            {
                limitRx1.Add(pair[0]);
                limitRx2.Add(pair[1]);
            }
            // Calc graph scaling in y and delta in x
            float scalingY = 1;
            if((max-min)!=0)
            {
                scalingY = (Globals.graphY - 2 * Globals.offsetY) / (max - min);
            }
            float offsetZero = Math.Abs(min * scalingY);
            float dx = (Globals.graphX - 2 * Globals.offsetX) / (nrPoints-1);
            float dy = (Globals.graphY - 2 * Globals.offsetY) / ((int)max - (int)min);
            float offsetRx = dx * nrPoints;
            float x1, y1; //, x2, y2 = 0;
            Pen p = new Pen(Brushes.Blue);
            Debug.WriteLine("minY,maxY = " + min.ToString() + ',' + max.ToString());
            Debug.WriteLine("Scaling in y = " + scalingY.ToString());
            Debug.WriteLine("Zero offset in y = " + offsetZero.ToString());
            // get averages of data
            List<float> nrTx  = new List<float>();
            List<float> nrRx  = new List<float>();
            List<float> avgTx = new List<float>();
            List<float> avgRx = new List<float>();
            // zero lists
            for (i = 0; i < nrPoints; i++)
            {
                nrTx.Add(0);
                nrRx.Add(0);
                avgTx.Add(0);
                avgRx.Add(0);
            }
            // add up all values
            foreach (float[] values in dataTx)
            {
                for (i = 0; i < values.Length; i++)
                {
                    avgTx[i] += values[i];
                    nrTx[i]++;
                }
            }
            foreach (float[] values in dataRx)
            {
                for (i = 0; i < values.Length; i++)
                {
                    avgRx[i] += values[i];
                    nrRx[i]++;
                }
            }
            // average then
            for (i = 0; i < avgTx.Count;i++)
            {
                if (nrTx[i] !=0)
                    avgTx[i] = avgTx[i] / nrTx[i];
            }
            for (i = 0; i < avgRx.Count; i++)
            {
                if (nrRx[i] != 0)
                    avgRx[i] = avgRx[i] / nrRx[i];
            }
            // y-grid
            m_g.DrawLine(Pens.Black, Globals.offsetX - 10f, Globals.graphY - Globals.offsetY + 2f, Globals.offsetX - 10f, Globals.offsetY - 2f);
            for (i = 0; i < nrPoints; i++)
            {
                x1 = Globals.offsetX + dx * i + 3;
                m_g.DrawLine(Pens.LightGray, x1, Globals.graphY - Globals.offsetY + 4, x1, Globals.offsetY - 2);
                m_g.DrawString(avgTx[i].ToString("F1"), font, Brushes.Blue, x1+10, Globals.graphY - Globals.offsetY + 20, drawFormat);
                m_g.DrawString(avgRx[i].ToString("F1"), font, Brushes.Red,  x1+10, Globals.graphY - Globals.offsetY + 10, drawFormat);
//                Debug.WriteLine("X1 = " + x1.ToString());
            }
            // x-grid
            m_g.DrawLine(Pens.Black, Globals.offsetX - 10, Globals.graphY - Globals.offsetY + 2, Globals.graphX - Globals.offsetX + 10, Globals.graphY - Globals.offsetY + 2);
            int idy = (int)max-(int)min;
            int noGrids = idy / Globals.deltaYGrid;
            for (i = 0; i <= noGrids; i++)
            {
//                y1 = (float)idy / noGrids * i * scalingY;
                y1 = (Globals.deltaYGrid * i)*scalingY;
                m_g.DrawLine(Pens.LightGray, Globals.offsetX - 12, Globals.graphY - Globals.offsetY - y1, Globals.graphX - Globals.offsetX + 10, Globals.graphY - Globals.offsetY - y1);
//                string ys = (min + (float)idy / (float)noGrids * (float)i).ToString("F1");
                string ys = ((int)min + Globals.deltaYGrid*i).ToString("F2");
                m_g.DrawString(ys, font, Brushes.Black, Globals.offsetX - 15, Globals.graphY - Globals.offsetY - y1 - 7, drawFormat);
            }
            // plot TX
            foreach (float[] values in dataTx)
                drawPoints("TX", p, values, dx, offsetZero, scalingY);
            // plot RX
            p.Brush = Brushes.Red;
            foreach (float[] values in dataRx)
                drawPoints("RX", p, values, dx, offsetZero, scalingY);
            // plot Limit
            drawPoints("liml", p, limitTx1.ToArray(), dx, offsetZero, scalingY);
            drawPoints("limu", p, limitTx2.ToArray(), dx, offsetZero, scalingY);
            drawPoints("liml", p, limitRx1.ToArray(), dx, offsetZero, scalingY);
            drawPoints("limu", p, limitRx2.ToArray(), dx, offsetZero, scalingY);
            m_g.DrawString("RX", Bfont, Brushes.Red, Globals.offsetX+10, (Globals.graphY - Globals.offsetY) * 0.25f, drawFormat);
            m_g.DrawString("TX", Bfont, Brushes.Blue, Globals.offsetX+10, (Globals.graphY - Globals.offsetY) * 0.75f, drawFormat);
            m_pbGraph.Image = myBitmap;
        }
        private void drawPoints(string type, Pen p, float[] values, float dx, float offsetZero, float scalingY)
        {                
            float x1, y1, x2, y2 = 0;
            if (type == "RX")
//                p.Brush = Globals.Instance.colors2[1];
                p.Brush = Globals.Instance.colors2[m_rnd.Next(Globals.Instance.colors2.Count())];
            else if (type == "TX")
//                p.Brush = Globals.Instance.colors1[1];
                p.Brush = Globals.Instance.colors1[m_rnd.Next(Globals.Instance.colors1.Count())];
            else if (type == "liml")   // limits
                p.Brush = Brushes.Red;
            else
                p.Brush = Brushes.Green;
            if (type.Contains("lim"))
                p.Width = 2;
            else
                p.Width = 1;
            for (int i = 0; i < values.Count()-1; i++)
            {
                y1 = Globals.graphY - Globals.offsetY - (offsetZero + values[i] * scalingY);
                x1 = Globals.offsetX + i * dx;
                y2 = Globals.graphY - Globals.offsetY - (offsetZero + values[i + 1] * scalingY);
                x2 = Globals.offsetX + (i+1) * dx;
                if (type.Contains("lim"))
                {
                    m_g.FillRectangle(p.Brush, x1-2, y1-2, 4, 4);
                    m_g.FillRectangle(p.Brush, x2-2, y2-2, 4, 4);
                }
                else
                {
                    m_g.FillEllipse(p.Brush, x1-3, y1-3, 6, 6);
                    m_g.FillEllipse(p.Brush, x2-3, y2-3, 6, 6);
                }
                m_g.DrawLine(p, x1, y1, x2, y2);
            }
            if (values.Count() == 1)
            {
                y1 = Globals.graphY - Globals.offsetY - (offsetZero + (float)values[0] * scalingY);
                x1 = Globals.offsetX;
                m_g.FillEllipse(p.Brush, x1-3, y1-3, 6, 6);
                Debug.WriteLine("Single " + type + " point added to graph");
            }
        }
        private void addTitleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Title t = new Title();
            t.ShowDialog();
            string title = t.GetTitle;
            Font Bfont = new System.Drawing.Font("Consolas", 18, FontStyle.Bold);
            m_g.FillRectangle(Brushes.White, Globals.offsetX, Globals.offsetY / 3, Globals.graphX - 2 * Globals.offsetX, Globals.offsetY / 2);
            m_g.DrawString(title, Bfont, Brushes.Black, Globals.offsetX, Globals.offsetY/4);
            m_pbGraph.Refresh();
        }
    }
}
