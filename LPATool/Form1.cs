﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Globalization;

namespace LPATool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Brush x = Globals.Instance.colors1[0];
            m_cbParam.Enabled = false;
            m_cbVersion.Items.Clear();
            foreach (string key in Globals.Instance.Versions.Keys)
                m_cbVersion.Items.Add(key);
            foreach (string key in Globals.Instance.Cases.Keys)
                m_cbCase.Items.Add(key);
            m_cbVersion.SelectedIndex = 0;
            m_cbCase.SelectedIndex = 0;
            this.Text = "Limmex Production Analysis Tool V" + Application.ProductVersion.ToString();
        }

        private string[] m_dataFiles = new string[0];
        /// <summary> Column header of data files, separated with ';'</summary>
        private string[] m_header = new string[0];
        /// <summary> Sub list of headers that are used for the analysis, i.e. the parameters</summary>
        private string[] m_param = new string[0];
        /// <summary>Contains all data lines of all files</summary>
        private List<string> m_data = new List<string>();
        /// <summary>Contains all data lines in arrays (line split with ';')</summary>
        private List<string[]> m_lists = new List<string[]>();
        /// <summary>Contains all filtered data lines in arrays</summary>
        private List<string[]> m_listsFiltered = new List<string[]>();
        /// <summary>Contains all limits from limit file</summary>
        private Dictionary<string, List<double>> m_limits = new Dictionary<string, List<double>>();
        /// <summary>Contains the values for each parameter. The array starts with min/max limits</summary>
        private Dictionary<string, List<double>> m_dict = new Dictionary<string, List<double>>();
        private Dictionary<string, double[]> m_results = new Dictionary<string, double[]>();
        private DoMath m_ma = new DoMath();
        private Graphics m_g;
        private bool externalConfigFile = false;
        private int m_lineLength = 0;
        /// <summary> Selected row index in case of cell value changed</summary>
        private int m_selectedRow = -1;
        /// <summary> Selected index of product version combo box</summary>
        private int m_oldIndex = 0;
        /// <summary> Stores the test programm version that the first data entry has </summary>
        private int m_iVersion = 0;
        /// <summary> Evaluate only the repairs </summary>
        private bool m_repairs = false;
        private bool m_dataFromDB = false;
        /// <summary> </summary>
        public bool DataFromDataBase
        {
            get { return m_dataFromDB; }
            set { m_dataFromDB = value; }
        }
        private SelectData m_selD;
        /// <summary> </summary>
        private void m_btnFiles_Click(object sender, EventArgs e)
        {
            m_selD = new SelectData(this);
            m_selD.Show();
            return;
        }
        /// <summary>Enable/disable certain gui elements</summary>
        public void enableButtons(bool enable)
        {
            m_btnWatch.Enabled = enable;
            m_btnID.Enabled = enable;
            calculatedFromLimitsToolStripMenuItem.Enabled = enable;
            showDetailedAudioFailsToolStripMenuItem1.Enabled = enable;
            searchWatchesByConditionToolStripMenuItem.Enabled = enable;
            showWatchIdsInFilteredDataToolStripMenuItem.Enabled = enable;
            perWeekToolStripMenuItem.Enabled = enable;
            perMonthToolStripMenuItem.Enabled = enable;
            drawGraphForGSMToolStripMenuItem.Enabled = enable;
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void selectParamterFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_openFileD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                m_header = File.ReadAllLines(m_openFileD.FileName);
                m_param = m_header[0].Split(';');
            }                                                      
        }
        private void loadConfigHeaderFiles()
        {
            List<string> file = new List<string>();
            if (Globals.Instance.Versions.TryGetValue(m_cbVersion.Text, out file))
            {   // load limits from config only, if no external file was selected
                if (!externalConfigFile && File.Exists(file[0]))
                {
                    string[] limits = File.ReadAllLines(file[0]);
                    loadLimits(limits);
                }
                else if (!externalConfigFile)
                {
                    MessageBox.Show("Fatal error: config file for selected version not found! Exiting ...", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                }
                if (File.Exists(file[1]))
                {
                    m_header = File.ReadAllLines(file[1]);
                    m_lineLength = m_header[0].Split(';').Count();
                    List<string> t = new List<string>(m_header[0].Split(';'));
                    for (int i = 0; i < Globals.indexStart; i++)
                        t.RemoveAt(0);
                    t.RemoveAt(t.Count() - 1); // remove last, empty entry
                    m_param = t.ToArray();
                }
                else
                {
                    MessageBox.Show("Fatal error: header file for selected version not found! Exiting ...", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Fatal error: config/header files for selected version not found! Exiting ...","Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }
        }
        private Object applyExcludeList(Dictionary<string,List<double>> data1, List<string> data2)
        {
            Object result = new Object();
            foreach (string name in Globals.Instance.ExcludeList)
            {
                if (data1!= null && data1.ContainsKey(name))
                        data1.Remove(name);
                else if (data2!= null && data2.Contains(name))
                        data2.Remove(name);
            }
            if (data1 != null)
                result = data1;
            else
                result = data2;
            return result;
        }
        public void storeDataFiles(string[] list)
        {
            m_dataFiles = list;
        }
        public void storeDataSets(List<string> list)
        {
            m_data = list;
        }
        public void loadData(bool autodetect)
        {
            if (!DataFromDataBase)
            {
                m_data.Clear();
                List<string> li = new List<string>();
                foreach (string file in m_dataFiles)
                {
                    Debug.WriteLine("Processing " + file);
                    li.Clear();
                    li = File.ReadAllLines(file).ToList();
                    // remove header line
                    li.RemoveAt(0);
                    Debug.WriteLine("Adding " + li.Count().ToString() + " sets of data");
                    //add to data list
                    m_data.AddRange(li);
                }
            }
            m_lists.Clear();
            // add data sets to lists if HW version match
            for (int i = 0; i < m_data.Count(); i++)
                m_lists.Add(m_data[i].Split(';'));
            // remove duplicated watch Ids
            if(m_cbDup.Checked == false)
                m_lists = filterMultipleWatchIds(m_lists);
//            saveData("ids");
            // autodetect HW version by taking first entry
            string key = "";
            if (autodetect && (m_lists.Count>0))
            {
                string version = m_lists[0][Globals.indexCfg];
                foreach (string k in Globals.Instance.Versions.Keys)
                {
                    if (version.Contains(k))
                    {
                        key = k;
                        break;
                    }
                }
            }
            if (key != "")
            {
                this.m_cbVersion.SelectedIndexChanged -= this.m_cbVersion_SelectedIndexChanged;
                m_cbVersion.Text = key;
                this.m_cbVersion.SelectedIndexChanged += new System.EventHandler(this.m_cbVersion_SelectedIndexChanged);
            }
            // filter HW version
            m_lists = filterData(Globals.indexCfg, new List<string>(new string[] { m_cbVersion.Text }), m_lists,true);
//            saveData("hw");
            if (m_repairs)
                m_lists = filterData(Globals.indexHW, new List<string>(new string[] { Globals.Repairs }), m_lists, false);
            else
            // filter repairs version
                m_lists = filterData(Globals.indexHW, new List<string>(new string[] { Globals.NoRepairs }), m_lists, false);
//            saveData("hw");
//            if (!aboveTesterVersion(Globals.tes(terVersion, m_lists))
                // remove lines w/o correct length for old tester format
//                m_lists = filterLineLength(m_lists);
            fillStatistics(true, m_lists);
            // filter case
            m_listsFiltered.Clear();
            m_listsFiltered = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Text], m_lists, false);
            loadAndCalc();
        }
        private void saveData(string ext)
        {
            string fileName = "data_"+ext+".csv";
            try
            {
                StreamWriter outfile = new StreamWriter(fileName, false);
                foreach (string[] entry in m_lists)
                {
                    string line = "";
                    foreach (string item in entry)
                        line += item + ";";
                    outfile.WriteLine(line);
                }
                outfile.Close();
            }
            catch {}
        }
        private void loadAndCalc()
        {
            if (m_listsFiltered.Count() > 0)
            {
                loadConfigHeaderFiles();
                loadDict(m_listsFiltered,true);
                addLimits();
                calcSelection();
                this.m_cbVersion.SelectedIndexChanged -= this.m_cbVersion_SelectedIndexChanged;
                m_oldIndex = m_cbVersion.SelectedIndex;
                this.m_cbVersion.SelectedIndexChanged += new System.EventHandler(this.m_cbVersion_SelectedIndexChanged);
            }
            else
            {
                MessageBox.Show("No data matching with selected filter. Please try again with different settings!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.m_cbVersion.SelectedIndexChanged -= this.m_cbVersion_SelectedIndexChanged;
                m_cbVersion.SelectedIndex = m_oldIndex;
                this.m_cbVersion.SelectedIndexChanged += new System.EventHandler(this.m_cbVersion_SelectedIndexChanged);
            }
        }
        private void loadDict(List<string[]> data, bool update)
        {
            m_dict.Clear();
            int max = m_header[0].Split(';').Count();
            for (int i = Globals.indexStart; i < max - Globals.skipLast; i++)
            {
                List<double> values = new List<double>();
                for (int j = 0; j < data.Count(); j++)
                    try
                    {
//                        Debug.WriteLine("loadDict: trying to convert value at index " + i.ToString() + ", string = " + data[j][i].ToString());
                        values.Add(double.Parse(data[j][i].ToString()));
                    }
                    catch
                    {
                        try
                        {
                            Debug.WriteLine("loadDict: Double parsing exception at index " + j.ToString() + " in data for value=" + data[j][i].ToString());
                        }
                        catch { }
                    }
                try
                {
                    m_dict.Add(m_param[i - Globals.indexStart], values);
//                    Debug.WriteLine("loadDict: adding index " + i.ToString() + ", dict length = " + m_dict.Count);
                }
                catch
                {
                    try
                    {
                        m_dict.Add(m_param[i - Globals.indexStart] + "_2", values);
                        Debug.WriteLine("loadData: Multiple key found for " + m_cbParam.Items[i - Globals.indexStart].ToString() + ", dict length = " + m_dict.Count);
                    }
                    catch { }
                }
            }
//            Debug.WriteLine("loadDict: dict length = " + m_dict.Count);
            if (update)
            {
                // remove entries of exclude list  
                m_dict = (Dictionary<string, List<double>>)applyExcludeList(m_dict, null);
                m_param = ((List<string>)applyExcludeList(null, m_param.ToList())).ToArray();
                loadParamsInForm(m_cbParam, true);
                m_cbParam.Enabled = true;
                m_cbParam.SelectedIndex = Globals.indexDefault;
            }
        }
        /// <summary>Filter line length</summary>
        private List<string[]> filterLineLength(List<string[]> data)
        {
            List<string[]> filteredData = new List<string[]>(data);
            List<int> indexArray = new List<int>();
            for(int i = 0; i < data.Count(); i++)
                // allow also lines that are one entry too short (sometimes last empty entry is missing...)
                if ((data[i].Count() != m_lineLength-1) && (data[i].Count() != m_lineLength))
                    if (!indexArray.Contains(i))
                        indexArray.Add(i);
            // make sure indices are top down before deleting lines
            indexArray.Sort();
            indexArray.Reverse();
            for (int i = 0; i < indexArray.Count(); i++)
            {
                Debug.WriteLine("Removing wrong line at index " + indexArray[i].ToString());
                filteredData.RemoveAt(indexArray[i]);
            }
            return filteredData;
        }
        /// <summary>Removes double watch ID from data, keeping only the last = newest</summary>
        private List<string[]> filterMultipleWatchIds(List<string[]> data)
        {
            List<string[]> filteredData = new List<string[]>(data);
            List<int> indexArray = new List<int>();
            // scan array backwards and keep last entry of each watchID
            for (int i = data.Count()-1; i > 0; i--)
            {
                string id = data[i][Globals.indexWatchID].TrimStart('0').Trim();
                for (int j = i-1; j >= 0; j--)
                    if (data[j][Globals.indexWatchID].TrimStart('0').Trim() == id)
                        if(!indexArray.Contains(j))
                            indexArray.Add(j);
            }
            // make sure indices are top down before deleting lines
            indexArray.Sort();
            indexArray.Reverse();
            for (int i = 0; i < indexArray.Count(); i++)
            {
                Debug.WriteLine("Removing duplicated watchID " + filteredData[indexArray[i]][Globals.indexWatchID].TrimStart('0').Trim() + " at index " + indexArray[i].ToString());
                filteredData.RemoveAt(indexArray[i]);
            }
            return filteredData;
        }
        /// <summary>Return subset from data where date is within week or month of year</summary>
        private List<string[]> filterDates(List<string[]> data, int weekMonth, int year, bool useAsWeek)
        {
            List<string[]> filteredData = new List<string[]>();
//            DateTime start = FirstDateOfWeekISO8601(year, week);
//            DateTime end = start.AddDays(7);
            foreach (string[] item in data)
            {
                string date = item[Globals.indexDate].Split('-')[0];
                int[] tmp = makeDate(date);
                DateTime dtDate = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
                // take all measurements of this week or month
                if (useAsWeek)
                {
                    if (GetIso8601WeekOfYear(dtDate) == weekMonth)
                        filteredData.Add(item);
                }
                else
                {
                    if (dtDate.Month == weekMonth)
                        filteredData.Add(item);
                }
            }            
            return filteredData;
        }
        /// <summary>Removes watch IDs from data that are not in list</summary>
        private List<string[]> filterWatchIds(List<string[]> data, List<string> list)
        {
            List<string[]> filteredData = new List<string[]>();
            List<int> indexArray = new List<int>();
            // scan array backwards and keep last entry of each watchID
            for (int i = data.Count()-1; i >= 0; i--)
            {
                string id = data[i][Globals.indexWatchID].TrimStart('0').Trim();
                for (int j = 0; j < list.Count(); j++)
                    if (list[j].TrimStart('0').Trim() == id)
                        if (!indexArray.Contains(i))
                            indexArray.Add(i);
            }
            for (int i = 0; i < indexArray.Count(); i++)
                filteredData.Add(data[indexArray[i]]);
            return filteredData;
        }
        /// <summary>Copy those lines that match filter to output</summary>
        private List<string[]> filterData(int index, List<string> filter, List<string[]> data, bool contains)
        {
            List<string[]> filteredData = new List<string[]>();
            List<string> localFilter = new List<string>();
            // filter depends on tester versions
            if (filter.Count() > 1)
            {
                if (aboveTesterVersion(Globals.testerVersion, data))
                {                       
                    localFilter.AddRange(new string[] { filter[2], filter[3] });
                }
                else
                {
                    localFilter.AddRange(new string[] { filter[0], filter[1] });
                }
            }
            // filter independent of tester versions
            else
                localFilter.AddRange(filter);

            bool limits = localFilter[0].Contains("0x");
            bool compare = (localFilter.Count() == 2 && Globals.Instance.CompList.Contains(localFilter[0]));
            long mask = 0;
            long maskEx = 0;
            long val = 0;
            bool notEqual = localFilter[0].Contains("!");
            if (notEqual)
            {   // remove '!'
                localFilter[0] = localFilter[0].Remove(0, 1);
            }
            if (limits)
            {
                localFilter[0] = localFilter[0].Split('x')[1];
                localFilter[1] = localFilter[1].Split('x')[1];
                long.TryParse(localFilter[0], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out mask);
                long.TryParse(localFilter[1], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out maskEx);
            }
            for (int i = 0; i < data.Count(); i++)
            {
                if (compare)
                {
                    bool flag = false;
                    double dat = 0;
                    double cmp = 0;
                    if ((data[i].Count() > index) && double.TryParse(data[i][index], out dat) && double.TryParse(localFilter[1], out cmp))
                    {
                        if (localFilter[0] == Globals.Instance.CompList[0])
                            flag = (dat > cmp);
                        else if (localFilter[0] == Globals.Instance.CompList[1])
                            flag = (dat >= cmp);
                        else if (localFilter[0] == Globals.Instance.CompList[2])
                            flag = (dat < cmp);
                        else if (localFilter[0] == Globals.Instance.CompList[3])
                            flag = (dat <= cmp);
                        else if (localFilter[0] == Globals.Instance.CompList[4])
                            flag = (dat == cmp);
                        else if (localFilter[0] == Globals.Instance.CompList[5])
                            flag = (dat != cmp);
                        if (flag)
                            filteredData.Add(data[i]);
                    }
                    else if (data[i].Count() > index)
                        Debug.WriteLine("filterData: Skipping data/value set " + data[i][index].ToString() + "/" + localFilter[1]);
                    else 
                        Debug.WriteLine("filterData: Skipping data@index/value set " + index.ToString() + "/" + localFilter[1]);
                }
                else if (limits)
                {
                    if (long.TryParse(data[i][index].ToString(), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out val))
                    {
                        val = val & (~Globals.VbatEnd5ErrorL);  // clear bit that corresponds to VbatEnd5Error
                        long value = val & mask;                // mask value with max, i.e. all bits set of all failing columns
                        long excl  = val & maskEx;              // mask with exclude mask, i.e. dependent errors
                        if (value > 0 && excl == 0)             // no errors allowed after exlude-masking
                            filteredData.Add(data[i]);
                    }
                }
                else if (localFilter[0].Length == 0)                                                                                     // no filtering
                    filteredData.Add(data[i]);
                else if (!notEqual)                                                                                                      // PASS
                {
                    if (localFilter[0] == Globals.Repairs && data[i][index].ToString() == localFilter[0])
                    {
                        filteredData.Add(data[i]);
                    }
                    else if (contains && data[i][index].ToString().Contains(localFilter[0]))                                            // match filter or filter is contained in data 
                    {
                        filteredData.Add(data[i]);
                    }
                    else if (data[i][index].ToString() == localFilter[0] || data[i][index].ToString() == Globals.VbatEnd5ErrorS)        // equal filter, mask vbattend+5"
                    {
                        int watch_id;
                        if (int.TryParse(data[i][Globals.indexWatchID], out watch_id))                                                 // we need a valid watch ID to allow a pass
                        {
                            filteredData.Add(data[i]);
                        }
                    }
                }
                else if (notEqual)                                                                                                      // FAILS
                { 
                    if (localFilter[0] == Globals.Repairs && data[i][index].ToString() != localFilter[0])
                    {
                        filteredData.Add(data[i]);
                    }
                    else if (data[i][index].ToString() != localFilter[0] && data[i][index].ToString() != Globals.VbatEnd5ErrorS)        // unequal filter, mask vbattend+5"
                    {
                        filteredData.Add(data[i]);
                    }
                }
                else
                    Debug.WriteLine("filterData: Skipping data (filter/value) = " + localFilter[0] + " / " + data[i][index].ToString());
            }
            return filteredData;
        }
        /// <summary>Calculates selected data and fills table</summary>
        private void calcSelection()
        {
            List<string> keys = new List<string>(getKeys());
            m_ma.loadData(m_dict);
            m_results = m_ma.calc(keys.ToArray());
            if(m_results.Count() > 0)
                fillTable(m_results, (int)m_results[m_results.Keys.ToList()[0]][(int)Globals.ResultItems.count]);
        }
        /// <summary>Load parameters into form</summary>
        private void loadParamsInForm(ComboBox cb, bool withAll)
        {
            int selIndex = cb.SelectedIndex;     // save selected index to reapply at the end
            cb.Items.Clear();
            if(withAll)
                cb.Items.Add("All");
            for (int i = 0; i < m_param.Count(); i++)
                cb.Items.Add(m_param[i]);
            cb.SelectedIndex = selIndex;
        }
        /// <summary>Load limits of given list</summary>
        private void loadLimits(string[] limits)
        {
            int i = 0;
            double min = 0.0;
            double max = 0.0;
            m_limits.Clear();
            while(i<limits.Count())
            {
                if (limits[i].Contains("[Firmware]"))
                { 
                }
                if (limits[i].Contains("[Battery]"))
                {
                    min = parseLine(limits[++i]);
                    max = parseLine(limits[++i]);
                    m_limits.Add("[Battery]", new List<double>(new double[] { min, max }));
                    m_limits.Add("[Battery2]", new List<double>(new double[] { min, max }));
                    m_limits.Add("[Battery3]", new List<double>(new double[] { min, max }));
                    i++;
                }
                else if (limits[i].Contains("[MeasSetup"))
                {
                    min = parseLine(limits[i + 4]);
                    max = parseLine(limits[i + 5]);
                    m_limits.Add(limits[i] + " RX", new List<double>(new double[] { min, max }));
                    min = parseLine(limits[i + 6]);
                    max = parseLine(limits[i + 7]);
                    m_limits.Add(limits[i] + " TX", new List<double>(new double[] { min, max }));
                    i += 7;
                }
                else if (limits[i].Contains("[GPSLimit"))
                {
                    min = parseLine(limits[i + 2]);
                    max = parseLine(limits[i + 3]);
                    m_limits.Add("[GPSLimit1]", new List<double>(new double[] { min, max }));
                    m_limits.Add("[GPSLimit2]", new List<double>(new double[] { min, max }));
                    i += 3;
                }
                else if (limits[i].Contains("[GSMHum]"))
                {
                    min = Globals.MIN;
                    max = parseLine(limits[++i]);
                    m_limits.Add("[GSMHum]", new List<double>(new double[] { min, max }));
                    i += 2;
                }
                else if (limits[i].Contains("[AudioLimit]"))
                {
                    min = parseLine(limits[i + 1]);
                    max = parseLine(limits[i + 2]);
                    m_limits.Add("Power", new List<double>(new double[] { min, max }));
                    min = parseLine(limits[i + 3]);
                    max = parseLine(limits[i + 4]);
                    m_limits.Add("dPower", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 5]);
                    m_limits.Add("k2 Tone", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 6]);
                    m_limits.Add("k3 Tone", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 7]);
                    m_limits.Add("k2 GSM", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 8]);
                    m_limits.Add("k3 GSM", new List<double>(new double[] { min, max }));
                    min = parseLine(limits[i + 9]);
                    max = Globals.MAX;
                    m_limits.Add("Tonemin", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 10]);
                    m_limits.Add("Duramax", new List<double>(new double[] { min, max }));
                    min = Globals.MIN;
                    max = parseLine(limits[i + 11]);
                    m_limits.Add("Noisemax", new List<double>(new double[] { min, max }));
                    i += 11;
                }
                else if (limits[i].Contains("[TPA]"))
                {
                    min = Globals.MIN;
                    max = parseLine(limits[i + 2]);
                    m_limits.Add("[TPA]", new List<double>(new double[] { min, max }));
                    i += 2;
                }
                else if (limits[i].Contains("[Alarm]"))
                {
                    min = parseLine(limits[i + 2]);
                    max = parseLine(limits[i + 3]);
                    m_limits.Add("[Alarm1]", new List<double>(new double[] { Globals.MIN, max*1000 }));
                    m_limits.Add("[Alarm2]", new List<double>(new double[] { min, Globals.MAX }));
                    m_limits.Add("[Alarm3]", new List<double>(new double[] { min, Globals.MAX }));
                    i += 3;
                }
                else
                    i++;
            }
        }
        /// <summary>Add limits to data</summary>
        private void addLimits()
        {
            foreach(Globals.limitParamMatch pair in Globals.Instance.LPMatch)
            {
                List<double> val = new List<double>();
                List<double> lim = new List<double>();
                if (m_dict.TryGetValue(pair.param, out val))
                {
                    if (m_limits.TryGetValue(pair.limit, out lim))
                        m_dict[pair.param].InsertRange(0, lim);
                    else
                        Debug.WriteLine("addLimits: key " + pair.limit + " not found in limit file!");
                }
                else
                    Debug.WriteLine("addLimits: key " + pair.param + " not found in data file!");
            }
        }
        /// <summary>Parse a line of data into doubles</summary>
        private double parseLine(string line)
        {
            string val = line.Split('=')[1].Trim().Split(' ')[0];
            double dval = 0.0;
            if (double.TryParse(val, out dval))
                return dval;
            else
                return -1;
        }
        private void m_cbParam_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> keys = new List<string>(getKeys());
            if (m_ma.hasData())
            {
                m_results = m_ma.calc(keys.ToArray());
                fillTable(m_results, m_data.Count());
            }
        }
        /// <summary>Fill table with statistics based on m_list</summary>
        private void fillStatistics(bool fromFiles, List<string[]> lists)
        {
            int all, pass, fail, fID, fBat1, fGSM, fAud, fGPS, fBat2, fAla, voi, fRes;
            double fIDp, fBat1p, fGSMp, fAudp, fGPSp, fBat2p, fAlap, fRESp;

            if (fromFiles)
            {
                m_dgvStat.Columns.Clear();
                m_dgvStat.Columns.Add("a", "");
                m_dgvStat.Columns.Add("b", "");
                m_dgvStat.Columns.Add("c", "");
                m_dgvStat.Columns.Add("d", "");
                m_dgvStat.Columns.Add("e", "");

                m_dgvStat.Columns.Add("f", "");
                m_dgvStat.Columns.Add("g", "");
                m_dgvStat.Columns.Add("h", "");
                m_dgvStat.Columns.Add("i", "");
            
                m_dgvStat.Columns.Add("k", "");
                m_dgvStat.Columns.Add("l", "");
                m_dgvStat.Columns.Add("m", "");
                m_dgvStat.Columns.Add("n", "");
                m_dgvStat.Columns.Add("p", "");
            
                m_dgvStat.Columns["b"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                m_dgvStat.Columns["d"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                m_dgvStat.Columns["g"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                m_dgvStat.Columns["i"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                m_dgvStat.Columns["m"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                m_dgvStat.Columns["p"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                List<int> values = new List<int>(getStatVals(lists).ToArray());

                fIDp   = (double)values[4]  / (double)values[3] * 100.0;
                fGSMp  = (double)values[5]  / (double)values[3] * 100.0;
                fAudp  = (double)values[6]  / (double)values[3] * 100.0;
                fGPSp  = (double)values[7]  / (double)values[3] * 100.0;
                fAlap  = (double)values[8]  / (double)values[3] * 100.0;
                fBat1p = (double)values[9]  / (double)values[3] * 100.0;
                fBat2p = (double)values[10] / (double)values[3] * 100.0;
                fRESp  = (double)values[11] / (double)values[3] * 100.0;

                m_dgvStat.Rows.Clear();
                m_dgvStat.Rows.Add(Globals.Instance.StatList[0], values[0], "", "100.0%", "",                                                           Globals.Instance.StatList[4], values[4], "", fIDp.ToString("F1")  + "%", "", Globals.Instance.StatList[8],  values[8],  "", fAlap.ToString("F1") + "%");
                m_dgvStat.Rows.Add(Globals.Instance.StatList[1], values[1], "", ((double)values[1] / (double)values[0] * 100).ToString("F1") + "%", "", Globals.Instance.StatList[5], values[5], "", fGSMp.ToString("F1") + "%", "", Globals.Instance.StatList[9],  values[9],  "", fBat1p.ToString("F1") + "%");
                m_dgvStat.Rows.Add(Globals.Instance.StatList[2], values[2], "", ((double)values[2] / (double)values[0] * 100).ToString("F1") + "%", "", Globals.Instance.StatList[6], values[6], "", fAudp.ToString("F1") + "%", "", Globals.Instance.StatList[10], values[10], "", fBat2p.ToString("F1") + "%");
                m_dgvStat.Rows.Add(Globals.Instance.StatList[3], values[3], "", ((double)values[3] / (double)values[0] * 100).ToString("F1") + "%", "", Globals.Instance.StatList[7], values[7], "", fGPSp.ToString("F1") + "%", "", Globals.Instance.StatList[11], values[11], "", fRESp.ToString("F1") + "%");
                m_dgvStat.Rows[1].Cells[3].Style.BackColor = Color.LightGreen;
                m_dgvStat.Rows[2].Cells[3].Style.BackColor = Color.LightGreen;
                m_dgvStat.Rows[3].Cells[3].Style.BackColor = Color.LightPink;
                m_dgvStat.AutoResizeColumns();
                m_dgvStat.AutoResizeRows();
            }
            else
            {   // clear list of failing watch IDs prior to calculate the yield
                m_ma.clearIDs();
                all = m_listsFiltered.Count();
                pass  = all-m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[1].ToString()]);
                fail  = all-pass;
                // clear list again, since the pass calculation filled the list with all fails!
                m_ma.clearIDs();
                fID   = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[3].ToString()]);
                fBat1 = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[4].ToString()]);
                fGSM  = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[5].ToString()]);
                fAud  = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[6].ToString()]);
                fGPS  = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[7].ToString()]);
                fBat2 = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[8].ToString()]);
                fAla  = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[9].ToString()]);
                fRes  = m_ma.calcYield(Globals.Instance.indexCases[m_cbCase.Items[10].ToString()]);
                if (fAla > -1)
                    voi = pass + fAla;
                else
                    voi = pass;
                Data dta = new Data();
                dta.loadStatistics(new List<int>(new int[]{all,pass,voi,fail,fID,fGSM,fAud,fGPS,fAla,fBat1,fBat2,fRes}));
                dta.StartPosition = FormStartPosition.Manual;
                dta.Show();
            }
        }
        /// <summary> Checks if the version string is above the given tVersion </summary>
        private bool aboveTesterVersion(int tVersion, List<string[]> data)
        {
            string version = "";
            if (data.Count() > 0)
            {
                version = data[0][Globals.indexVersion].Split(',')[0];
                try
                {
                    m_iVersion = int.Parse(version.Split('.')[0]) * 100 + int.Parse(version.Split('.')[1]) * 10 + int.Parse(version.Split('.')[2]);
                }
                catch { return false; }
            }
            else
                return false;
            return (m_iVersion > tVersion);
        }
        /// <summary> calculates the statistics </summary>
        private List<int> getStatVals(List<string[]> lists)
        {   // indices must match with order in Globals.Instance.Cases!!
            int all = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[0].ToString()], lists, false).Count();
            int pass = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[1].ToString()], lists, false).Count();
            int exl = filterData(Globals.indexError, Globals.Instance.Cases["Exclude"], lists, false).Count();
            int fail = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[2].ToString()], lists, false).Count();
            // correction for excluded bit 41
            pass += exl;
            fail -= exl;
            int fID = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[3].ToString()], lists, false).Count();
            int fBat1 = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[4].ToString()], lists, false).Count();
            int fGSM = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[5].ToString()], lists, false).Count();
            int fAud = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[6].ToString()], lists, false).Count();
            int fGPS = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[7].ToString()], lists, false).Count();
            int fBat2 = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[8].ToString()], lists, false).Count();
            int fAla = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[9].ToString()], lists, false).Count();
            int fRes = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[10].ToString()], lists, false).Count();
            int voi   = pass + fAla;

            return new List<int>(new int[] { all, pass, voi, fail, fID, fGSM, fAud, fGPS, fAla, fBat1, fBat2, fRes });
        }
        /// <summary>Fill table with given result(s)</summary>
        private void fillTable(Dictionary<string, double[]> result, int noSamples)
        {   // save first visible index to be able to restore latter
            int fIndex = m_dgv.FirstDisplayedScrollingRowIndex;
            m_dgv.Columns.Clear();
            m_dgv.Columns.Add("name", "Name");
            m_dgv.Columns.Add("min val", "Min value");
            m_dgv.Columns.Add("avg", "Average");
            m_dgv.Columns.Add("sigma", "Sigma");
            m_dgv.Columns.Add("max val", "Max value");
            m_dgv.Columns.Add("cpk", "CPK");
            m_dgv.Columns.Add("min lim", "Min limit");
            m_dgv.Columns.Add("max lim", "Max limit");

            m_dgv.Columns["name"].ReadOnly    = true;
            m_dgv.Columns["min val"].ReadOnly = true;
            m_dgv.Columns["avg"].ReadOnly     = true;
            m_dgv.Columns["sigma"].ReadOnly   = true;
            m_dgv.Columns["max val"].ReadOnly = true;
            m_dgv.Columns["cpk"].ReadOnly     = true; 
            m_dgv.Rows.Clear();
            //            m_lblSamples.Text = "Number of samples = " + noSamples.ToString();
            double[] val = new double[0];
            foreach (string key in result.Keys)
            {
                val = new double[0];
                result.TryGetValue(key, out val);

                m_dgv.Rows.Add(new string[] { key, val[(int)Globals.ResultItems.min].ToString("F2"), val[(int)Globals.ResultItems.average].ToString("F2"), val[(int)Globals.ResultItems.sigma].ToString("F2"), val[(int)Globals.ResultItems.max].ToString("F2"), val[(int)Globals.ResultItems.cpk].ToString("F2"), val[(int)Globals.ResultItems.minLimit].ToString("F2"), val[(int)Globals.ResultItems.maxLimit].ToString("F2") });
                if (val[(int)Globals.ResultItems.cpk] < Globals.CPK)
                    m_dgv.Rows[m_dgv.Rows.Count - 2].Cells[5].Style.BackColor = Color.LightPink;
                else
                    m_dgv.Rows[m_dgv.Rows.Count - 2].Cells[5].Style.BackColor = Color.LightGreen;
            }
            m_dgv.AutoResizeColumns();

            string name = "";
            if (m_cbParam.Text == "All")
            {
                string key = "";
                if (m_selectedRow>0)
                    key = m_dgv.Rows[m_selectedRow].Cells[0].Value.ToString();
                if (result.TryGetValue(key, out val))
                    name = key;
            }
            else if (m_cbParam.Text != "" && result.TryGetValue(m_cbParam.Text, out val))
                name = m_cbParam.Text;
            else if (result.TryGetValue(m_cbParam.Items[Globals.indexDefault].ToString(), out val))
                name = m_cbParam.Items[Globals.indexDefault].ToString();

            if (name != "")
            {
                double minL = val[(int)Globals.ResultItems.minLimit];
                double maxL = val[(int)Globals.ResultItems.maxLimit];
                drawHistogram(val.SubArray((int)Globals.ResultItems.histogramValues, Globals.histogramSize), val[(int)Globals.ResultItems.delta],
                              val[(int)Globals.ResultItems.min], minL, maxL, name, "Number of samples = " + val[(int)Globals.ResultItems.count].ToString("F0"));
            }
            if(fIndex >= 0 && m_dgv.RowCount > fIndex)
                m_dgv.FirstDisplayedScrollingRowIndex = fIndex;
        }
        /// <summary>Draw the histogram for the given values</summary>
        private void drawHistogram(double[] values, double delta, double min, double minL, double maxL, string name, string comment)
        {
            Bitmap myBitmap = new Bitmap(Globals.imageFile);
            m_g = Graphics.FromImage(myBitmap);
            Font Bfont = new System.Drawing.Font("Consolas", 10, FontStyle.Bold);
            Font font  = new System.Drawing.Font("Consolas", 10, FontStyle.Regular);
            // Set format of string.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
            float scalingY = (Globals.histoY-2*Globals.offsetY) / (float)values.Max();
            float w = (Globals.histoX-2*Globals.offsetX)/(float)Globals.histogramSize-Globals.deltaX;
            float h = 0f;
            float x = 0f;
            for (int i = 0; i < values.Count(); i++)
            {
                h = (float)values[i] * scalingY;
                if (h == 0) h = 1;
                x = Globals.offsetX + i * (w + Globals.deltaX);
                m_g.FillRectangle(Brushes.BlueViolet, x, Globals.histoY - Globals.offsetY - h, w, h);
                m_g.DrawString((min+(double)i * delta).ToString("F2"), font, Brushes.Black, x-6, Globals.histoY - Globals.offsetY + 2f, drawFormat);
            }
            drawLimits(min, min + values.Count()*delta, minL, maxL);
            drawYaxis(values.Max(), scalingY, font, drawFormat);
            m_g.DrawString(name, Bfont, Brushes.Black, 1f, Globals.offsetY / 4);
            m_g.DrawString(comment, font, Brushes.Black, 1f, Globals.offsetY / 2);
            m_pbHisto.Image = myBitmap;
        }
        /// <summary>Draws the limits if inside x-axis range</summary>
        private void drawLimits(double min, double max, double minL, double maxL)
        {
            double d = (max - min);
            if(min <= minL && d > 0)
            {
                float dx = Globals.offsetX + (Globals.histoX - 2 * Globals.offsetX) * (float)((minL-min)/d);
                m_g.DrawLine(Pens.OrangeRed,dx,Globals.histoY - Globals.offsetY, dx, Globals.offsetY);
            }
            if (maxL <= max && d > 0)
            {
                float dx = Globals.offsetX + (Globals.histoX - 2 * Globals.offsetX) * (float)((maxL-min)/d);
                m_g.DrawLine(Pens.Red,dx,Globals.histoY - Globals.offsetY, dx, Globals.offsetY);
            }
        }
        /// <summary>Draws the y axis into the histogram image</summary>
        private void drawYaxis(double max, float scaling, Font font, StringFormat drawFormat)
        {
            m_g.DrawLine(Pens.Black, Globals.offsetX - 10f, Globals.histoY - Globals.offsetY + 2f, Globals.offsetX - 10f, Globals.offsetY - 2f);
            float dy  = (float)max * scaling / (float)Globals.histogramYticks;
            float dyn = (float)max / (float)Globals.histogramYticks;
            if (!float.IsNaN(dy))
            {
                for (int i = 0; i <= Globals.histogramYticks; i++)
                {
                    m_g.DrawLine(Pens.Black, Globals.offsetX - 12f, Globals.offsetY + i * dy, Globals.offsetX - 8f, Globals.offsetY + i * dy);
                    m_g.DrawString((i * dyn).ToString("F1"), font, Brushes.Black, 1f, Globals.histoY - (Globals.offsetY + 8f + i * dy));
                }
//                m_g.DrawLine(Pens.Black, Globals.offsetX - 12f, Globals.histoY - Globals.offsetY, Globals.offsetX - 8f, Globals.histoY - Globals.offsetY);
            }
        }
        /// <summary>Loads the selected limit file</summary>
        private void selectConfigFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_openFileD2.ShowDialog() == System.Windows.Forms.DialogResult.OK && m_openFileD2.FileName.Length > 0)
                {
                    string[] config = File.ReadAllLines(m_openFileD2.FileName);
                    loadLimits(config);
                    externalConfigFile = true;
                    if (m_dataFiles.Count() > 0)
                        loadData(false);
                }
        }
        /// <summary>Callback for the HW version: reloads data from file for the selected version</summary>
        private void m_cbVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadConfigHeaderFiles();
            if ((!DataFromDataBase && (m_dataFiles.Count() > 0)) || (DataFromDataBase && (m_data.Count() > 0))) 
            {
                loadData(false);
            }
        }
        /// <summary>Callback for table: draws the histogram for the parameter of the clicked row</summary>
        private void m_dgv_CellContentChanged(object sender, DataGridViewCellEventArgs e)
        {
            string key = m_dgv.Rows[e.RowIndex].Cells[0].Value.ToString();
            double nMin = 0;
            double nMax = 0;
//            double oMin = m_dict[key][0];
//            double oMax = m_dict[key][1];
            // save selected row to be able to update histgram 
            m_selectedRow = e.RowIndex;

            if (double.TryParse(m_dgv.Rows[e.RowIndex].Cells[6].Value.ToString(), out nMin))
            {
                if (double.TryParse(m_dgv.Rows[e.RowIndex].Cells[7].Value.ToString(), out nMax))
                {   // change limits
                    m_dict[key][0] = nMin;
                    m_dict[key][1] = nMax;
                    m_ma.loadData(m_dict);
                    List<string> keys = new List<string>(getKeys());
                    m_results = m_ma.calc(keys.ToArray());
                    fillTable(m_results, m_data.Count());
                }
            }
        }
        /// <summary>Return the parameter names (=keys of m_dict) depending on the selection in m_cbParam</summary>
        private string[] getKeys()
        {
            if (m_cbParam.SelectedIndex == 0)
                return m_dict.Keys.ToArray();
            else if (m_cbParam.SelectedIndex > 0)
                return new string[] { m_cbParam.Items[m_cbParam.SelectedIndex].ToString() };
            else
                return new string[] { m_cbParam.Items[Globals.indexDefault].ToString() };
        }
        /// <summary>Callback for table: draws the histogram for the parameter of the clicked row</summary>
        private void m_dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < m_cbParam.Items.Count-1 && e.ColumnIndex == 0)
            {
                double[] val = new double[0];
//                if (m_results.TryGetValue(m_cbParam.Items[e.RowIndex + 1].ToString(), out val))
                if (m_results.TryGetValue(m_dgv.Rows[e.RowIndex].Cells[0].Value.ToString(), out val))
                {
                    double minL = val[(int)Globals.ResultItems.minLimit];
                    double maxL = val[(int)Globals.ResultItems.maxLimit];
                    drawHistogram(val.SubArray((int)Globals.ResultItems.histogramValues, Globals.histogramSize), val[(int)Globals.ResultItems.delta],
                                  val[(int)Globals.ResultItems.min], minL, maxL, m_cbParam.Items[e.RowIndex + 1].ToString(), "Number of samples = " + val[(int)Globals.ResultItems.count].ToString("F0"));
                }
                else
                    MessageBox.Show("No data for histogram!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
//            else
//                MessageBox.Show("No data in row", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        /// <summary>Saves the histgram to a file</summary>
        private void m_pbHisto_Click(object sender, EventArgs e)
        {
            m_saveFileImage.ShowDialog();
            if(m_saveFileImage.FileName !="")
                m_pbHisto.Image.Save(m_saveFileImage.FileName);
        }
        /// <summary>Callback for case: re-calculate data, filtered according to the selected error class</summary>
        private void m_cbCase_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_dataFiles.Count() > 0 || (DataFromDataBase && (m_data.Count() > 0)))
            {
                // filter case
                m_listsFiltered.Clear();
                m_listsFiltered = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Text], m_lists, false);
                if (m_cbCase.Text == "Only PASS")
                {
                    List<string[]> exluded = new List<string[]>();
                    exluded = filterData(Globals.indexError, Globals.Instance.Cases["Exclude"], m_lists, false);
                    m_listsFiltered.AddRange(exluded);
                }
                loadAndCalc();
            }
        }
        /// <summary>Displays the measurement results for the selected watch </summary>
        private void m_btnWatch_Click(object sender, EventArgs e)
        {
            if (m_tbID.Text != "" && m_data.Count() > 0)
            {
                string data = "";
                foreach(string[] watch in m_lists)
                {
                    if (watch[Globals.indexWatchID].TrimStart('0').Trim() == m_tbID.Text.TrimStart('0').Trim())
                    {
                        List<string> t = new List<string>(watch);
                        t.RemoveAt(t.Count() - 1); // remove last, empty entry
                        foreach(string item in t)
                            data += item + ";";
                        data.Remove(data.Length - 1);
                        break;
                    }
                }
                if (data.Length > 0)
                {
//                    if (data.Split(';').Length == m_header[0].Split(';').Length)
//                    {
                        Data dta = new Data();
//                        int a = data.Split(';').Length;
//                        int b = m_header[0].Split(';').Length;

                        dta.loadData(data, m_header[0], "Measurement data for ID" + data[Globals.indexWatchID],"Name", "Value");
                        dta.StartPosition = FormStartPosition.Manual;
                        dta.Show();
//                    }
//                    else
//                        MessageBox.Show("Data inconsistent with header!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                    MessageBox.Show("Watch ID not found in data!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        /// <summary>Filters the data for the Watch-Ids contained in the selected file </summary>
        private void m_btnID_Click(object sender, EventArgs e)
        {
            if (m_openFileD3.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                List<string> iDsS = new List<string>(File.ReadAllLines(m_openFileD3.FileName));
                m_listsFiltered = filterWatchIds(m_listsFiltered, iDsS);
                fillStatistics(true, m_listsFiltered);
                loadAndCalc();
            }
        }
        private int getParamIndex(string item)
        {
            return m_header[0].Split(';').ToList().IndexOf(item);     
        }
        private void showWatchIds(List<string[]> list)
        {
            string foundIDs = "";
            string foundDates = "";
            if (list.Count() > 0)
            {
                foreach (string[] item in list)
                {
                    foundIDs += item[Globals.indexWatchID] + ";";
                    foundDates += item[Globals.indexDate] + ";";
                }
                foundIDs = foundIDs.Remove(foundIDs.Length - 1);
                foundDates = foundDates.Remove(foundDates.Length - 1);
                Data dta = new Data();
                dta.loadData(foundDates, foundIDs, "Watch IDs and measurement date", "Watch ID", "Date-Time");
                dta.loadData(m_data, m_header);
                dta.StartPosition = FormStartPosition.Manual;
                dta.Sort(0);
                dta.Show();
            }
            else
                MessageBox.Show("No watches found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        /// <summary> Returns YYMMDD as [YY,MM,DD] </summary>
        private int[] makeDate(string date)
        {
            int[] result = new int[3];
            if (date.Length == 6)
            {
                string y = date.Substring(0, 2);
                string m = date.Substring(2, 2);
                string d = date.Substring(4, 2);
                int.TryParse(y, out result[0]);
                int.TryParse(m, out result[1]);
                int.TryParse(d, out result[2]);
            }
            return result;
        }
        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        private int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        private void m_cbRepairs_CheckedChanged(object sender, EventArgs e)
        {
            m_repairs = m_cbRepairs.Checked;
            loadData(false);
        }
        /// <summary>Searches watches meeting a certain condition </summary>
        private void searchWatchesByConditionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Search s = new Search();
            loadParamsInForm(s.Params, false);
            if (m_cbParam.SelectedIndex > 0)
                s.Params.SelectedIndex = m_cbParam.SelectedIndex - 1;
            else
                s.Params.SelectedIndex = 0;
            s.ShowDialog();
            int index = getParamIndex(s.Params.SelectedItem.ToString());
            if (index > 0)
            {   // double filter-entries because we currently have two tester versions
                List<string[]> list = filterData(index, new List<string>(new string[] { s.Comparison, s.Value, s.Comparison, s.Value }), m_listsFiltered, false);
                showWatchIds(list);
            }
            else
                MessageBox.Show("Parameter index out of range", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void drawGraphForGSMToolStripMenuItem_Click(object sender, EventArgs e)
        {
//            Graph gr = new Graph((Form)sender);
            Graph gr = new Graph(this);
            gr.drawGraph(m_listsFiltered, m_header, m_limits);
            gr.Show();
        }

        private void showWatchIdsInFilteredDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showWatchIds(m_listsFiltered);
        }
        /// <summary> Analyses yield per week for m_listsFiltered</summary>
        private void perWeekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<List<string[]>> weekList = new List<List<string[]>>();
            List<List<int>> weekDataList = new List<List<int>>();
            List<int> weeks = new List<int>();
            List<string> dates = new List<string>();
            // get only dates
            foreach (string[] item in m_listsFiltered)
                dates.Add(item[Globals.indexDate]);
            // get min and max dates for sorting into weeks
            string date = dates.Min().Split('-')[0];
            int[] tmp = makeDate(date);
            DateTime first = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            date = dates.Max().Split('-')[0];
            tmp = makeDate(date);
            DateTime last = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            // sort data into weekList
            int start = GetIso8601WeekOfYear(first);
            int stop = GetIso8601WeekOfYear(last);
            if (first.Year == last.Year)
            {
                for (int i = 0; i < (stop - start + 1); i++)
                {
                    weekList.Add(filterDates(m_listsFiltered, start + i, first.Year, true));
                    weeks.Add(start + i);
                }
                foreach (List<string[]> weekData in weekList)
                    weekDataList.Add(getStatVals(weekData));

                Data dta = new Data();
                dta.loadWeeklyStats(weekDataList, weeks);
                dta.StartPosition = FormStartPosition.Manual;
                dta.Show();
            }

        }
        /// <summary> Analyses yield per month for m_listsFiltered</summary>
        private void perMonthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<List<string[]>> monthList = new List<List<string[]>>();
            List<List<int>> monthDataList = new List<List<int>>();
            List<int> months = new List<int>();
            List<string> dates = new List<string>();
            // get only dates
            foreach (string[] item in m_listsFiltered)
                dates.Add(item[Globals.indexDate]);
            // get min and max dates for sorting into weeks
            string date = dates.Min().Split('-')[0];
            int[] tmp = makeDate(date);
            DateTime first = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            date = dates.Max().Split('-')[0];
            tmp = makeDate(date);
            DateTime last = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            // sort data into weekList
            int start = first.Month;
            int stop = last.Month;
            if (first.Year == last.Year)
            {
                for (int i = 0; i < (stop - start + 1); i++)
                {
                    monthList.Add(filterDates(m_listsFiltered, start + i, first.Year, false));
                    months.Add(start + i);
                }
                foreach (List<string[]> monthData in monthList)
                    monthDataList.Add(getStatVals(monthData));

                Data dta = new Data();
                dta.loadMonthlyStats(monthDataList, months);
                dta.StartPosition = FormStartPosition.Manual;
                dta.Show();
            }

        }
        /// <summary>Calculates and displays the yield using the limits instead of the error codes</summary>
        private void calculatedFromLimitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_cbParam.SelectedIndex = 0;
            m_ma.loadData(m_listsFiltered);
            m_ma.loadHeaders(m_header[0].Split(';'));
            //fill statistics 
            fillStatistics(false, m_lists);
        }
        /// <summary>Shows the detailed fails of the audio test </summary>
        private void showDetailedAudioFailsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            List<int> figures = new List<int>(Globals.Instance.AudioCases.Count());
            List<string> names = new List<string>(Globals.Instance.AudioCases.Count());
            int audioSamples;
            audioSamples = filterData(Globals.indexError, Globals.Instance.Cases[m_cbCase.Items[6].ToString()], m_listsFiltered, false).Count();
            foreach (string key in Globals.Instance.AudioCases.Keys)
            {
                names.Add(key);
                figures.Add(filterData(Globals.indexError, Globals.Instance.AudioCases[key], m_listsFiltered, false).Count());
            }
            Data dta = new Data();
            dta.loadAudioStat(names, figures, audioSamples);
            dta.StartPosition = FormStartPosition.Manual;
            dta.Show();
        }

        private void m_cbDup_CheckedChanged(object sender, EventArgs e)
        {
            loadData(false);
        }

        private void showAveragesOverTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<List<string[]>> weekList = new List<List<string[]>>();
            List<Dictionary<string, double[]>> weekAveragesList = new List<Dictionary<string, double[]>>();
            List<int> weeks = new List<int>();
            List<string> dates = new List<string>();
            // get only dates
            foreach (string[] item in m_listsFiltered)
                dates.Add(item[Globals.indexDate]);
            // get min and max dates for sorting into weeks
            string date = dates.Min().Split('-')[0];
            int[] tmp = makeDate(date);
            DateTime first = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            date = dates.Max().Split('-')[0];
            tmp = makeDate(date);
            DateTime last = new DateTime(tmp[0], tmp[1], tmp[2], new GregorianCalendar());
            // sort data into weekList
            int start = GetIso8601WeekOfYear(first);
            int stop = GetIso8601WeekOfYear(last);
            int nrWeeks =  stop - start + 1;

            if (first.Year != last.Year)
            {
                nrWeeks = 52 - start + stop;                   
            }

            for (int i = 0; i < nrWeeks; i++)
            {
                int r = 0;
                int y = Math.DivRem((start + i),52,out r);
                int w = (start + i) % 51;
                if (w == 0) { w=51; }
                weekList.Add(filterDates(m_listsFiltered, w, first.Year + y, true));
                weeks.Add(w);
            }
            // make sure m_param contains all headers (i.e. excludeList not executed)
            // since m_param is used in loadDict;
            loadConfigHeaderFiles();
            string[] keys = new List<string>(getKeys()).ToArray();
            foreach (List<string[]> weekData in weekList)
            {
                loadDict(weekData, false);
                addLimits();
                m_ma.loadData(m_dict);
                Dictionary<string, double[]> result = new Dictionary<string, double[]>(m_ma.calc(keys)); 
                weekAveragesList.Add(result);
            }
            Data dta = new Data();
            dta.loadAverages(weekAveragesList, weeks);
            dta.StartPosition = FormStartPosition.Manual;
            dta.Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {   // open pdf file containing the description of the tool
            System.Diagnostics.Process.Start("Help.pdf");
        }
    }
    public static class myArray
    {
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            if (data != null)
            {
                Array.Copy(data, index, result, 0, length);
                return result;
            }
            return null;
        }
    }
}
