﻿namespace LPATool
{
    partial class SelectData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_cbDB = new System.Windows.Forms.CheckBox();
            this.m_dtpStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_dtpStop = new System.Windows.Forms.DateTimePicker();
            this.m_btnFiles = new System.Windows.Forms.Button();
            this.m_openFileD = new System.Windows.Forms.OpenFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.m_cbTesters = new System.Windows.Forms.ComboBox();
            this.m_lblDB = new System.Windows.Forms.Label();
            this.m_openFileD4 = new System.Windows.Forms.OpenFileDialog();
            this.m_btnSetupFile = new System.Windows.Forms.Button();
            this.m_lblHost = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_cbDB
            // 
            this.m_cbDB.AutoSize = true;
            this.m_cbDB.Checked = true;
            this.m_cbDB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_cbDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_cbDB.Location = new System.Drawing.Point(13, 13);
            this.m_cbDB.Name = "m_cbDB";
            this.m_cbDB.Size = new System.Drawing.Size(74, 20);
            this.m_cbDB.TabIndex = 0;
            this.m_cbDB.Text = "Use DB";
            this.m_cbDB.UseVisualStyleBackColor = true;
            this.m_cbDB.CheckedChanged += new System.EventHandler(this.m_cbDB_CheckedChanged);
            // 
            // m_dtpStart
            // 
            this.m_dtpStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.m_dtpStart.Location = new System.Drawing.Point(102, 43);
            this.m_dtpStart.Name = "m_dtpStart";
            this.m_dtpStart.Size = new System.Drawing.Size(183, 22);
            this.m_dtpStart.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Start date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Stop date";
            // 
            // m_dtpStop
            // 
            this.m_dtpStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_dtpStop.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.m_dtpStop.Location = new System.Drawing.Point(102, 71);
            this.m_dtpStop.Name = "m_dtpStop";
            this.m_dtpStop.Size = new System.Drawing.Size(183, 22);
            this.m_dtpStop.TabIndex = 3;
            // 
            // m_btnFiles
            // 
            this.m_btnFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_btnFiles.Location = new System.Drawing.Point(13, 129);
            this.m_btnFiles.Name = "m_btnFiles";
            this.m_btnFiles.Size = new System.Drawing.Size(272, 24);
            this.m_btnFiles.TabIndex = 5;
            this.m_btnFiles.Text = "Select data";
            this.m_btnFiles.UseVisualStyleBackColor = true;
            this.m_btnFiles.Click += new System.EventHandler(this.m_btnFiles_Click);
            // 
            // m_openFileD
            // 
            this.m_openFileD.DefaultExt = "csv";
            this.m_openFileD.FileName = "140923-Pass_0.csv";
            this.m_openFileD.Filter = "CSV-Dateien|*.csv";
            this.m_openFileD.Multiselect = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tester name";
            // 
            // m_cbTesters
            // 
            this.m_cbTesters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_cbTesters.FormattingEnabled = true;
            this.m_cbTesters.Location = new System.Drawing.Point(102, 99);
            this.m_cbTesters.Name = "m_cbTesters";
            this.m_cbTesters.Size = new System.Drawing.Size(183, 24);
            this.m_cbTesters.TabIndex = 8;
            // 
            // m_lblDB
            // 
            this.m_lblDB.AutoSize = true;
            this.m_lblDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblDB.Location = new System.Drawing.Point(12, 168);
            this.m_lblDB.MaximumSize = new System.Drawing.Size(270, 16);
            this.m_lblDB.MinimumSize = new System.Drawing.Size(270, 16);
            this.m_lblDB.Name = "m_lblDB";
            this.m_lblDB.Size = new System.Drawing.Size(270, 16);
            this.m_lblDB.TabIndex = 10;
            this.m_lblDB.Text = "DB name = ";
            // 
            // m_openFileD4
            // 
            this.m_openFileD4.DefaultExt = "csv";
            this.m_openFileD4.FileName = "Setup.xml";
            this.m_openFileD4.Filter = "XML-Dateien|*.xml";
            this.m_openFileD4.Multiselect = true;
            // 
            // m_btnSetupFile
            // 
            this.m_btnSetupFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_btnSetupFile.Location = new System.Drawing.Point(102, 13);
            this.m_btnSetupFile.Name = "m_btnSetupFile";
            this.m_btnSetupFile.Size = new System.Drawing.Size(183, 24);
            this.m_btnSetupFile.TabIndex = 11;
            this.m_btnSetupFile.Text = "Select new DB setup file";
            this.m_btnSetupFile.UseVisualStyleBackColor = true;
            this.m_btnSetupFile.Click += new System.EventHandler(this.m_btnSetupFile_Click);
            // 
            // m_lblHost
            // 
            this.m_lblHost.AutoSize = true;
            this.m_lblHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblHost.Location = new System.Drawing.Point(12, 192);
            this.m_lblHost.MaximumSize = new System.Drawing.Size(270, 16);
            this.m_lblHost.MinimumSize = new System.Drawing.Size(270, 16);
            this.m_lblHost.Name = "m_lblHost";
            this.m_lblHost.Size = new System.Drawing.Size(270, 16);
            this.m_lblHost.TabIndex = 12;
            this.m_lblHost.Text = "Host name = ";
            // 
            // SelectData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 217);
            this.Controls.Add(this.m_lblHost);
            this.Controls.Add(this.m_btnSetupFile);
            this.Controls.Add(this.m_lblDB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_cbTesters);
            this.Controls.Add(this.m_btnFiles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_dtpStop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_dtpStart);
            this.Controls.Add(this.m_cbDB);
            this.Name = "SelectData";
            this.Text = "SelectData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_cbDB;
        private System.Windows.Forms.DateTimePicker m_dtpStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker m_dtpStop;
        private System.Windows.Forms.Button m_btnFiles;
        private System.Windows.Forms.OpenFileDialog m_openFileD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox m_cbTesters;
        private System.Windows.Forms.Label m_lblDB;
        private System.Windows.Forms.OpenFileDialog m_openFileD4;
        private System.Windows.Forms.Button m_btnSetupFile;
        private System.Windows.Forms.Label m_lblHost;
    }
}