﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LPATool
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
            m_cbCompare.Items.Clear();
            foreach (string item in Globals.Instance.CompList)
                m_cbCompare.Items.Add(item);
            m_cbCompare.SelectedIndex = 0;
        }
        /// <summary> Combox for parameter selection </summary>
        public ComboBox Params
        {
            get { return m_cbParam; }
            set { m_cbParam = value; }
        }
        /// <summary> Name of selected parameter </summary>
        public string Parameter
        {
            get { return m_cbParam.Text; }
        }
        /// <summary> Name of selected comparison </summary>
        public string Comparison
        {
            get { return m_cbCompare.Text; }
        }
        /// <summary> Value to be compared </summary>
        public string Value
        {
            get { return m_tbVal.Text; }
        }
        private void m_btnS_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
