﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;

namespace LPATool
{
    [Serializable]
    public class Setup
    {
        public Setup() { }
        public Setup(string host, string db, string tn, string us, string pw, List<string> testers)
        {
            m_hostName = host;
            m_databaseName = db;
            m_tableName = tn;
            m_user = us;
            m_pwd = pw;
            m_testerNames = testers;
        }
        private string m_hostName;
        private string m_databaseName;
        private string m_tableName;
        private string m_user;
        private string m_pwd;
        private List<string> m_testerNames;

        public string HostName
        {
            get { return m_hostName; }
            set { m_hostName = value; }
        }
        public string DatabaseName
        {
            get { return m_databaseName; }
            set { m_databaseName = value; }
        }
        public string TableName
        {
            get { return m_tableName; }
            set { m_tableName = value; }
        }
        public string User
        {
            get { return m_user; }
            set { m_user = value; }
        }
        public string Pwd
        {
            get { return m_pwd; }
            set { m_pwd = value; }
        }
        public List<string> TesterNames
        {
            get { return m_testerNames; }
            set { m_testerNames = value; }
        }

        #region XML-File-Operations
        /// <summary> Writes the configuration list to XML file. </summary>
        public static bool WriteConfigListToXmlFile(Setup setup, string filename)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(Setup));
                XmlTextWriter tw = new XmlTextWriter(filename, Encoding.UTF8);

                ser.Serialize(tw, setup);
                tw.Close();
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("WriteConfigListToXmlFile: error = " + ex.ToString());
                return false;
            }
        }

        /// <summary> Reads the configuration list from XML file. </summary>
        public static Setup ReadConfigListFromXmlFile(string filename)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(Setup));
                XmlTextReader rdr = new XmlTextReader(filename);

                Setup setup;

                setup = ser.Deserialize(rdr) as Setup;
                rdr.Close();

                return setup;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ReadConfigListFromXmlFile: error = " + ex.ToString());
                return null;
            }
        }
        #endregion
    }
}
