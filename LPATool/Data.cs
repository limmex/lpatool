﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace LPATool
{
    public partial class Data : Form
    {
        public Data()
        {
            InitializeComponent();
        }
        /// <summary> Contains the complete measurement data</summary>
        private List<string> m_lists = new List<string>();
        /// <summary> Contains the header strings</summary>
        private string[] m_header;
        private bool m_showAverages = false;
        /// <summary> Holds the week numbers for averages </summary>
        private List<int> m_weeks;
        /// <summary> Holds the weekly data from averages </summary>
        private List<Dictionary<string, double[]>> m_weeklyData;
        /// <summary> Load found watch IDs into table</summary>
        public void loadData(string data, string header, string title, string col0, string col1)
        {
            m_dgvData.Columns.Clear();
            m_dgvData.Rows.Clear();
            m_dgvData.Columns.Add("a", col0);
            m_dgvData.Columns.Add("b", col1);
            m_dgvData.Columns["a"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            m_dgvData.Columns["b"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            List<string> he = new List<string>(header.Split(';'));
            List<string> da = new List<string>();
            //            if (data.GetType() == string) //Type.GetType(System.String.Empty))
            da = new List<string>(data.ToString().Split(';'));
            //            else
            //                da = (List<string>)data;
            for (int i = 0; i < Math.Min(he.Count(), da.Count()); i++)
                m_dgvData.Rows.Add(he[i], da[i]);
            m_dgvData.AutoResizeColumns();
            this.Text = title;
        }
        /// <summary> Load given data and header into members </summary>
        public void loadData(List<string> data, string[] head)
        {
            m_lists = data;
            m_header = head;
        }
        /// <summary> Data contains per week values for all,pass,voi,fail,fID,fGSM,fAud,fGPS,fAla,fBat1,fBat2 </summary>
        public void loadWeeklyStats(List<List<int>> data, List<int> weeks)
        {
            if (data.Count() == weeks.Count())
            {
                setupTable(weeks, true);
                for (int j = 0; j < weeks.Count(); j++)
                    addStatRows(data, j);
            }
        }
        /// <summary> Data contains per month values for all,pass,voi,fail,fID,fGSM,fAud,fGPS,fAla,fBat1,fBat2 </summary>
        public void loadMonthlyStats(List<List<int>> data, List<int> month)
        {
            if (data.Count() == month.Count())
            {
                setupTable(month, false);
                for (int j = 0; j < month.Count(); j++)
                    addStatRows(data, j);
            }
        }
        /// <summary> Adds data rows into statistical overview (monthly or weekly)</summary>
        private void addStatRows(List<List<int>> data, int j)
        {
            m_dgvData.Rows[0].Cells[j + 1].Value = data[j][0].ToString();
            for (int i = 1; i < data[j].Count(); i++)
            {
                if (i < 4)
                    m_dgvData.Rows[i].Cells[j + 1].Value = ((float)data[j][i] / (float)data[j][0] * 100.0).ToString("F1") + "%";
                else
                    m_dgvData.Rows[i].Cells[j + 1].Value = ((float)data[j][i] / (float)data[j][3] * 100.0).ToString("F1") + "%";
            }
        }
        /// <summary> Adds rows of average values into statistical overview (weekly)</summary>
        private void addAverages(int index)
        {   // add title
            this.Text = m_cbSelection.Text + " per week";
            m_showAverages = true;
            for (int j = 0; j < m_weeklyData.Count; j++) // selects the column
            {
                for (int i = 0; i < m_weeklyData[j].Keys.Count; i++)
                {
                    string key = m_weeklyData[j].Keys.ElementAt(i);
                    m_dgvData.Rows[i].Cells[j + 1].Value = ((float)m_weeklyData[j][key][index]).ToString("F2");
                }
            }
        }
        /// <summary> Data contains values for all,pass,voi,fail,fID,fGSM,fAud,fGPS,fAla,fBat1,fBat2 </summary>
        public void loadStatistics(List<int> data)
        {
            setupTable();
            for (int i = 0; i < Globals.Instance.StatList.Count(); i++)
            {
                if (i < 4)
                    if (data[i] < 0)
                        m_dgvData.Rows.Add(Globals.Instance.StatList[i], "?", "?");
                    else
                        m_dgvData.Rows.Add(Globals.Instance.StatList[i], data[i].ToString(), ((float)data[i] / (float)data[0] * 100.0).ToString("F2"));
                else
                {
                    if (data[i] < 0)
                        m_dgvData.Rows.Add(Globals.Instance.StatList[i], "?", "?");
                    else
                        m_dgvData.Rows.Add(Globals.Instance.StatList[i], data[i].ToString(), ((float)data[i] / (float)data[3] * 100.0).ToString("F2"));
                }
            }
            m_dgvData.AutoResizeColumns();
            this.Text = "Statistics using limits";
        }
        /// <summary> Shows the statistics of the audio fails</summary>
        public void loadAudioStat(List<string> names, List<int> data, int nrAudioSamples)
        {
            setupTable();
            if (names.Count() != data.Count())
            {
                MessageBox.Show("Error: number of names does not match with the numbers of data! Exiting ...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                m_btnClose_Click(this, EventArgs.Empty);
            }

            m_dgvData.Rows.Add("Audio", nrAudioSamples.ToString(), "100.00");

            for (int i = 0; i < names.Count(); i++)
            {
                m_dgvData.Rows.Add(names[i], data[i].ToString(), ((float)data[i] / (float)nrAudioSamples * 100.0).ToString("F2"));
            }
            m_dgvData.AutoResizeColumns();
            this.Text = "Detailed statistics for audio fails";
        }
        /// <summary> Shows the statistics of the averages per week</summary>
        public void loadAverages(List<Dictionary<string, double[]>> data, List<int> weeks)
        {
            m_weeks = weeks;
            m_weeklyData = data;
            if (data.Count() == weeks.Count())
            {
                // be sure to pass longest set of data to set up table. Else not enough row are generated....
                int max = 0;
                int index = 0;
                for (int i = 0; i < data.Count; i++) 
                {
                    if (max < data[i].Count)
                    {
                        index = i;
                        max = data[i].Count;
                    }
                }
                // setup table and fill-in data
                setupTable(weeks, data[index]);
            }
        }
        public void Sort(int column)
        {
            if (m_dgvData.ColumnCount > column)
                m_dgvData.Sort(m_dgvData.Columns[column], ListSortDirection.Ascending);
        }
        private void setupTable()
        {
            m_dgvData.Columns.Clear();
            m_dgvData.Rows.Clear();
            m_dgvData.Columns.Add("a", "Name");
            m_dgvData.Columns.Add("b", "Amount");
            m_dgvData.Columns.Add("c", "[%]");
            m_dgvData.Columns["a"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            m_dgvData.Columns["b"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            m_dgvData.Columns["c"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        /// <summary> Setup table for average view per week </summary>
        private void setupTable(List<int> weekMonths, Dictionary<string, double[]> data)
        {
            addColumns(weekMonths, true);
            // enable selection and fill combo box
            m_lblSel.Visible = true;
            m_cbSelection.Visible = true;
            foreach (string item in Globals.Instance.ResultItemNames)
            {
                m_cbSelection.Items.Add(item);
            }
            // add rows before filling the table
            foreach (string key in data.Keys)
                m_dgvData.Rows.Add(key);
            // set index which triggers loading the data into the table 
            m_cbSelection.SelectedIndex = (int)Globals.ResultItems.average;
        }
        /// <summary> Setup table for yield view per week or month </summary>
        private void setupTable(List<int> weekMonths, bool useAsWeek)
        {
            addColumns(weekMonths, useAsWeek);
            for (int i = 0; i < Globals.Instance.StatList.Count(); i++)
                m_dgvData.Rows.Add(Globals.Instance.StatList[i]);
        }
        /// <summary> Add Columns for weeks or month </summary>
        private void addColumns(List<int> weekMonths, bool useAsWeek)
        {
            m_dgvData.Columns.Clear();
            m_dgvData.Rows.Clear();
            m_dgvData.Columns.Add("a", "Name");
            foreach (int Nr in weekMonths)
            {
                string name = "";
                if (useAsWeek)
                    name = "Week " + Nr.ToString("D2");
                else
                    name = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Nr); ;
                m_dgvData.Columns.Add(name, name);
                m_dgvData.Columns[name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            m_dgvData.Columns["a"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        /// <summary>Callback for table: shows the measurement data of the watch</summary>
        private void m_dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex>-1) && (e.RowIndex < m_dgvData.RowCount - 1))
            {
                string id = m_dgvData.Rows[e.RowIndex].Cells[0].Value.ToString().TrimStart('0').Trim();
                if (m_showAverages)
                {   // plot row
                    List<float> data = new List<float>();
                    DataGridViewRow row = new DataGridViewRow();
                    row = m_dgvData.Rows[e.RowIndex];
                    for (int i = 1; i < row.Cells.Count; i++)
                    {
                        float val = 0;
                        if ((row.Cells[i].Value!=null) && float.TryParse(row.Cells[i].Value.ToString(), out val))
                        {
                            data.Add(val);
                        }
                        else data.Add(-99);
                    }
                    Graph gr = new Graph(this);
                    gr.drawGraph(data, id, m_weeks);
                    gr.Show();
                }
                else
                {   // print watch data
                    string data = "";
                    foreach (string watch in m_lists)
                    {
                        if (watch.Split(';')[Globals.indexWatchID].TrimStart('0').Trim() == id)
                        {
                            data = watch;
                            break;
                        }
                    }
                    if (data.Length > 0)
                    {
                        Data dta = new Data();

                        dta.loadData(data, m_header[0], "Measurement data for ID" + data.Split(';')[Globals.indexWatchID], "Name", "Value");
                        dta.StartPosition = FormStartPosition.Manual;
                        dta.Show();
                    }
                }
            }
        }
        private void m_btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void m_cbSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(m_cbSelection.SelectedIndex > -1)
                addAverages(m_cbSelection.SelectedIndex);
        }
    }
}
