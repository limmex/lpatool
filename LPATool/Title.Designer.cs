﻿namespace LPATool
{
    partial class Title
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_tbTitle = new System.Windows.Forms.TextBox();
            this.m_btnOK = new System.Windows.Forms.Button();
            this.m_btnCn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_tbTitle
            // 
            this.m_tbTitle.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tbTitle.Location = new System.Drawing.Point(12, 12);
            this.m_tbTitle.Name = "m_tbTitle";
            this.m_tbTitle.Size = new System.Drawing.Size(260, 32);
            this.m_tbTitle.TabIndex = 0;
            this.m_tbTitle.Text = "Title";
            // 
            // m_btnOK
            // 
            this.m_btnOK.Location = new System.Drawing.Point(114, 51);
            this.m_btnOK.Name = "m_btnOK";
            this.m_btnOK.Size = new System.Drawing.Size(75, 23);
            this.m_btnOK.TabIndex = 1;
            this.m_btnOK.Text = "OK";
            this.m_btnOK.UseVisualStyleBackColor = true;
            this.m_btnOK.Click += new System.EventHandler(this.m_btnOK_Click);
            // 
            // m_btnCn
            // 
            this.m_btnCn.Location = new System.Drawing.Point(196, 51);
            this.m_btnCn.Name = "m_btnCn";
            this.m_btnCn.Size = new System.Drawing.Size(75, 23);
            this.m_btnCn.TabIndex = 2;
            this.m_btnCn.Text = "Cancel";
            this.m_btnCn.UseVisualStyleBackColor = true;
            this.m_btnCn.Click += new System.EventHandler(this.m_btnCn_Click);
            // 
            // Title
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 80);
            this.Controls.Add(this.m_btnCn);
            this.Controls.Add(this.m_btnOK);
            this.Controls.Add(this.m_tbTitle);
            this.Name = "Title";
            this.Text = "Title";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_tbTitle;
        private System.Windows.Forms.Button m_btnOK;
        private System.Windows.Forms.Button m_btnCn;
    }
}