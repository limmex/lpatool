﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace LPATool
{
    class DoMath
    {
        public DoMath() {}

        /// <summary>double[] format: min imit, max limit, values[] </summary>
        private Dictionary<string, List<double>> m_data = new Dictionary<string, List<double>>();
        /// <summary>double[] format: Count, min, average, sigma, max, cpk, min limit, max limit, histogram </summary>
        private Dictionary<string, double[]> m_result = new Dictionary<string, double[]>();
        /// <summary>Contains the data in an array, one line per watch</summary>
        private List<string[]> m_lists = new List<string[]>();
        /// <summary>Contains the header line used in the data file, i.e. Params_V1.x-yz.csv</summary>
        private List<string> m_headers = new List<string>();
        /// <summary>Contains the watch IDs of the failed data sets in order to avoid counting fails twice</summary>
        private List<string> m_failedIDs = new List<string>();

        public void clearIDs()
        {
            m_failedIDs.Clear();
        }
        public void loadHeaders(string[] data)
        {
            m_headers = new List<string>(data);
        }
        public void loadData(List<string[]> data)
        {
            m_lists = data;
        }
        public void loadData(Dictionary<string, List<double>> data)
        {
            m_data = data;
            m_result.Clear();
        }
        public bool hasData()
        {
            return (m_data.Count>0);
        }
        public Dictionary<string, double[]> calc(string[] keys)
        {
            m_result.Clear();
            foreach (string key in keys)
            {
                List<double> vals = new List<double>();
                if (m_data.TryGetValue(key, out vals) && vals.Count >= 3)       // >=3: min/max and at least one value
                {
                    //                    Debug.WriteLine("Number of data values = " + vals.Count().ToString());
                    double min = vals[0];
                    double max = vals[1];
                    // copy and remove limits (first two entries) for calculations
                    List<double> t = new List<double>(vals);
                    t.RemoveAt(0);
                    t.RemoveAt(0);
                    double[] data = new double[t.Count()];
                    t.CopyTo(data);
                    double ave = avg(data);
                    double sig = sigma(data);
                    m_result.Add(key, new double[] { t.Count(), data.Min(), ave, sig, data.Max(), cpk(ave, sig, min, max), min, max });
                    Dictionary<string, double[]> tmp = new Dictionary<string, double[]>();
                    tmp.Add(key, makeHistogram(data));
                    m_result[key] = m_result[key].Concat(tmp[key]).ToArray();

                }
                else
                {
                    if (vals.Count < 3)
                    {
                        Debug.WriteLine("Incomplete data (no limits)!");
                    }
                    else
                    {
                        Debug.WriteLine("DoMath.calc: Key " + key + " not found in data!");
                    }
                }
            }
            return m_result;
        }
        private double avg(double[] values)
        {
            double sum = 0;
            foreach (double val in values)
                sum += val;
            return sum/values.Count();
        }
        private double sigma(double[] values)
        {
            double variance = 0.0;
            double average= avg(values);
            foreach (double val in values)
                variance += (val - average) * (val - average);
            return Math.Sqrt(variance / values.Count());
        }
        private double cpk(double avg, double sigma, double min, double max)
        {
            return (Math.Min(avg-min, max-avg))/3.0/sigma;
        }
        private double[] makeHistogram(double[] values)
        {
            double[] histo = new double[Globals.histogramSize+1];
            double min = values.Min();
            double max = values.Max();
            double delta = (max-min)/(Globals.histogramSize-1);
            histo[0] = delta;
            for (int i = 0; i < Globals.histogramSize; i++)
            {
                histo[i+1] = 0.0;
                foreach (double d in values)
                {
                    if((min+delta*i<=d) & (d<min+delta*(i+1)))
                        histo[i+1]++;
                }
            }
            return histo;
        }
        public int calcYielold(List<int> indices)
        {
            int min = int.MaxValue;
            Dictionary<string, int> passList = new Dictionary<string,int>();
            foreach (int index in indices)
            {   // no yield calculation if index is -1.
                if (index != -1)
                {
                    string key = Globals.Instance.LPMatch[index].param;
                    int ix = m_headers.IndexOf(key);

                    if (m_data.ContainsKey(key))
                    {
                        double minL = m_data[key][0];
                        double maxL = m_data[key][1];
                        passList.Add(key, 0);
                        for (int i = 2; i < m_data[key].Count(); i++)
                        {
                            if ((minL <= m_data[key][i]) && (m_data[key][i] <= maxL))
                                passList[key]++;
                        }
                    }
                    else
                    {
                        Debug.WriteLine("calcYield: key " + key + " not found in data!");
                        if (!passList.ContainsKey("None"))
                            passList.Add("None", m_data[Globals.Instance.LPMatch[0].param].Count() - 2);
                    }
                }
                else
                {   // not limit returns all as pass (taking amount given for first param in list)
                    passList.Add("None", m_data[Globals.Instance.LPMatch[0].param].Count() - 2);
                    break;
                }
            }
            foreach (string key in passList.Keys)
                if (passList[key] < min)
                    min = passList[key];
            return min;
        }
        /// <summary> Calculates the yield using the given indices to look up the parameters </summary>
        public int calcYield(List<int> indices)
        {
            int failed = 0;
            foreach (string[] watch in m_lists)
            {
                foreach (int index in indices)
                {   // no yield calculation if index is -1.
                    if (index != -1)
                    {
                        string key = Globals.Instance.LPMatch[index].param;
                        int ix = m_headers.IndexOf(key);
                        // exception if key contains '_2': used next measurement data instead
                        if(key.Contains("_2"))
                            ix = m_headers.IndexOf(key.Remove(17,2)) + 3;

                        if (m_data.ContainsKey(key) && ix > -1)
                        {   // normal tests having limits
                            if (index < 31)
                            {
                                double minL = m_data[key][0];
                                double maxL = m_data[key][1];
                                double val = 0.0;
                                try
                                {
                                    double.TryParse(watch[ix], out val);
                                    if (!((minL <= val) && (val <= maxL)))
                                    {
                                        if (!m_failedIDs.Contains(watch[Globals.indexWatchID]))
                                        {
                                            failed++;
                                            m_failedIDs.Add(watch[Globals.indexWatchID]);
                                            Debug.WriteLine("Key " + key + " failed for watch " + watch[Globals.indexWatchID] + ", min/val/max = " + minL.ToString() + "/" + val.ToString() + "/" + maxL.ToString());
                                            break;
                                        }
                                    }
                                }
                                catch
                                {
                                    try
                                    {
                                        Debug.WriteLine("Parsing for key " + key + " of watch " + watch[Globals.indexWatchID] + " failed. Value = " + watch[ix]);
                                    }
                                    catch { }
                                    failed++;
                                    break;
                                }
                            }
                            else
                            {
                                bool pass = true;
                                switch (index)
                                {
                                    case 31:    int val = 0;
                                                try
                                                {
                                                    int.TryParse(watch[ix], out val);

                                                }
                                                catch { pass = false; }
                                                break;
                                    case 32:    if (watch[ix] != "ss")
                                                    pass = false;
                                                break;
                                    case 33:    if (!(watch[ix].StartsWith("35") && watch[ix].Length == 15))
                                                    pass = false;
                                                break;
                                    case 34:    if (!(watch[ix].StartsWith("89") && watch[ix].Length == 20))
                                                    pass = false;
                                                break;
                                    case 35:    if (watch[ix].Length != 15)
                                                    pass = false;
                                                break;
                                    case 36: break;
                                }
                                if (!m_failedIDs.Contains(watch[Globals.indexWatchID]) && !pass)
                                {
                                    failed++;
                                    m_failedIDs.Add(watch[Globals.indexWatchID]);
                                    Debug.WriteLine("Key " + key + " failed for watch " + watch[Globals.indexWatchID]);
                                    break;
                                }
                            }
                        }
                        else
                            Debug.WriteLine("calcYield: key " + key + " not found in data!");
                    }
                    else
                        return -1;
                }
            }
            return failed;
        }
    }
}

