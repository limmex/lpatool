﻿namespace LPATool
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.m_openFileD = new System.Windows.Forms.OpenFileDialog();
            this.m_btnFiles = new System.Windows.Forms.Button();
            this.m_cbParam = new System.Windows.Forms.ComboBox();
            this.m_lblParam = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.selectLimitFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectParamterFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchWatchesByConditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawGraphForGSMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showWatchIdsInFilteredDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDetailedAudioFailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.showAveragesOverTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yieldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perWeekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perMonthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatedFromLimitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_dgv = new System.Windows.Forms.DataGridView();
            this.m_openFileD2 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.m_cbVersion = new System.Windows.Forms.ComboBox();
            this.m_pbHisto = new System.Windows.Forms.PictureBox();
            this.m_saveFileImage = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.m_cbCase = new System.Windows.Forms.ComboBox();
            this.m_dgvStat = new System.Windows.Forms.DataGridView();
            this.m_btnWatch = new System.Windows.Forms.Button();
            this.m_tbID = new System.Windows.Forms.TextBox();
            this.m_btnID = new System.Windows.Forms.Button();
            this.m_openFileD3 = new System.Windows.Forms.OpenFileDialog();
            this.m_cbRepairs = new System.Windows.Forms.CheckBox();
            this.m_cbDup = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbHisto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvStat)).BeginInit();
            this.SuspendLayout();
            // 
            // m_openFileD
            // 
            this.m_openFileD.DefaultExt = "csv";
            this.m_openFileD.FileName = "140923-Pass_0.csv";
            this.m_openFileD.Filter = "CSV-Dateien|*.csv";
            this.m_openFileD.Multiselect = true;
            // 
            // m_btnFiles
            // 
            this.m_btnFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_btnFiles.Location = new System.Drawing.Point(13, 27);
            this.m_btnFiles.Name = "m_btnFiles";
            this.m_btnFiles.Size = new System.Drawing.Size(117, 24);
            this.m_btnFiles.TabIndex = 0;
            this.m_btnFiles.Text = "Select data files";
            this.m_btnFiles.UseVisualStyleBackColor = true;
            this.m_btnFiles.Click += new System.EventHandler(this.m_btnFiles_Click);
            // 
            // m_cbParam
            // 
            this.m_cbParam.Enabled = false;
            this.m_cbParam.FormattingEnabled = true;
            this.m_cbParam.Location = new System.Drawing.Point(273, 103);
            this.m_cbParam.Name = "m_cbParam";
            this.m_cbParam.Size = new System.Drawing.Size(144, 21);
            this.m_cbParam.TabIndex = 1;
            this.m_cbParam.SelectedIndexChanged += new System.EventHandler(this.m_cbParam_SelectedIndexChanged);
            // 
            // m_lblParam
            // 
            this.m_lblParam.AutoSize = true;
            this.m_lblParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblParam.Location = new System.Drawing.Point(272, 86);
            this.m_lblParam.Name = "m_lblParam";
            this.m_lblParam.Size = new System.Drawing.Size(145, 16);
            this.m_lblParam.TabIndex = 2;
            this.m_lblParam.Text = "Analysis for parameter:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolsToolStripMenuItem,
            this.yieldToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectLimitFileToolStripMenuItem,
            this.selectParamterFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // selectLimitFileToolStripMenuItem
            // 
            this.selectLimitFileToolStripMenuItem.Name = "selectLimitFileToolStripMenuItem";
            this.selectLimitFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.selectLimitFileToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.selectLimitFileToolStripMenuItem.Text = "Select Config File";
            this.selectLimitFileToolStripMenuItem.Click += new System.EventHandler(this.selectConfigFileToolStripMenuItem_Click);
            // 
            // selectParamterFileToolStripMenuItem
            // 
            this.selectParamterFileToolStripMenuItem.Name = "selectParamterFileToolStripMenuItem";
            this.selectParamterFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.selectParamterFileToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.selectParamterFileToolStripMenuItem.Text = "Select Parameter File";
            this.selectParamterFileToolStripMenuItem.Click += new System.EventHandler(this.selectParamterFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.searchWatchesByConditionToolStripMenuItem,
            this.drawGraphForGSMToolStripMenuItem,
            this.showWatchIdsInFilteredDataToolStripMenuItem,
            this.showDetailedAudioFailsToolStripMenuItem1,
            this.showAveragesOverTimeToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // searchWatchesByConditionToolStripMenuItem
            // 
            this.searchWatchesByConditionToolStripMenuItem.Enabled = false;
            this.searchWatchesByConditionToolStripMenuItem.Name = "searchWatchesByConditionToolStripMenuItem";
            this.searchWatchesByConditionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.searchWatchesByConditionToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.searchWatchesByConditionToolStripMenuItem.Text = "Search watches by condition";
            this.searchWatchesByConditionToolStripMenuItem.Click += new System.EventHandler(this.searchWatchesByConditionToolStripMenuItem_Click);
            // 
            // drawGraphForGSMToolStripMenuItem
            // 
            this.drawGraphForGSMToolStripMenuItem.Enabled = false;
            this.drawGraphForGSMToolStripMenuItem.Name = "drawGraphForGSMToolStripMenuItem";
            this.drawGraphForGSMToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.G)));
            this.drawGraphForGSMToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.drawGraphForGSMToolStripMenuItem.Text = "Draw graph for GSM";
            this.drawGraphForGSMToolStripMenuItem.Click += new System.EventHandler(this.drawGraphForGSMToolStripMenuItem_Click);
            // 
            // showWatchIdsInFilteredDataToolStripMenuItem
            // 
            this.showWatchIdsInFilteredDataToolStripMenuItem.Enabled = false;
            this.showWatchIdsInFilteredDataToolStripMenuItem.Name = "showWatchIdsInFilteredDataToolStripMenuItem";
            this.showWatchIdsInFilteredDataToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.I)));
            this.showWatchIdsInFilteredDataToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.showWatchIdsInFilteredDataToolStripMenuItem.Text = "Show watch Id\'s in filtered data";
            this.showWatchIdsInFilteredDataToolStripMenuItem.Click += new System.EventHandler(this.showWatchIdsInFilteredDataToolStripMenuItem_Click);
            // 
            // showDetailedAudioFailsToolStripMenuItem1
            // 
            this.showDetailedAudioFailsToolStripMenuItem1.Enabled = false;
            this.showDetailedAudioFailsToolStripMenuItem1.Name = "showDetailedAudioFailsToolStripMenuItem1";
            this.showDetailedAudioFailsToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.showDetailedAudioFailsToolStripMenuItem1.Size = new System.Drawing.Size(271, 22);
            this.showDetailedAudioFailsToolStripMenuItem1.Text = "Show detailed audio fails";
            this.showDetailedAudioFailsToolStripMenuItem1.Click += new System.EventHandler(this.showDetailedAudioFailsToolStripMenuItem1_Click);
            // 
            // showAveragesOverTimeToolStripMenuItem
            // 
            this.showAveragesOverTimeToolStripMenuItem.Name = "showAveragesOverTimeToolStripMenuItem";
            this.showAveragesOverTimeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.O)));
            this.showAveragesOverTimeToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.showAveragesOverTimeToolStripMenuItem.Text = "Show averages over time";
            this.showAveragesOverTimeToolStripMenuItem.Click += new System.EventHandler(this.showAveragesOverTimeToolStripMenuItem_Click);
            // 
            // yieldToolStripMenuItem
            // 
            this.yieldToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perWeekToolStripMenuItem,
            this.perMonthToolStripMenuItem});
            this.yieldToolStripMenuItem.Name = "yieldToolStripMenuItem";
            this.yieldToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.yieldToolStripMenuItem.Text = "Yield";
            // 
            // perWeekToolStripMenuItem
            // 
            this.perWeekToolStripMenuItem.Enabled = false;
            this.perWeekToolStripMenuItem.Name = "perWeekToolStripMenuItem";
            this.perWeekToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.W)));
            this.perWeekToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.perWeekToolStripMenuItem.Text = "per week";
            this.perWeekToolStripMenuItem.Click += new System.EventHandler(this.perWeekToolStripMenuItem_Click);
            // 
            // perMonthToolStripMenuItem
            // 
            this.perMonthToolStripMenuItem.Enabled = false;
            this.perMonthToolStripMenuItem.Name = "perMonthToolStripMenuItem";
            this.perMonthToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.M)));
            this.perMonthToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.perMonthToolStripMenuItem.Text = "per month";
            this.perMonthToolStripMenuItem.Click += new System.EventHandler(this.perMonthToolStripMenuItem_Click);
            // 
            // calculatedFromLimitsToolStripMenuItem
            // 
            this.calculatedFromLimitsToolStripMenuItem.Enabled = false;
            this.calculatedFromLimitsToolStripMenuItem.Name = "calculatedFromLimitsToolStripMenuItem";
            this.calculatedFromLimitsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Y)));
            this.calculatedFromLimitsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.calculatedFromLimitsToolStripMenuItem.Text = "calculated from limits";
            this.calculatedFromLimitsToolStripMenuItem.Click += new System.EventHandler(this.calculatedFromLimitsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // m_dgv
            // 
            this.m_dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.m_dgv.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.m_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.m_dgv.Location = new System.Drawing.Point(12, 130);
            this.m_dgv.Name = "m_dgv";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.m_dgv.RowHeadersVisible = false;
            this.m_dgv.ShowCellErrors = false;
            this.m_dgv.ShowCellToolTips = false;
            this.m_dgv.ShowEditingIcon = false;
            this.m_dgv.ShowRowErrors = false;
            this.m_dgv.Size = new System.Drawing.Size(627, 547);
            this.m_dgv.TabIndex = 4;
            this.m_dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgv_CellContentClick);
            this.m_dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.m_dgv_CellContentChanged);
            // 
            // m_openFileD2
            // 
            this.m_openFileD2.DefaultExt = "cfg";
            this.m_openFileD2.Filter = "CFG-Datei|*.cfg";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Product version";
            // 
            // m_cbVersion
            // 
            this.m_cbVersion.FormattingEnabled = true;
            this.m_cbVersion.Location = new System.Drawing.Point(14, 103);
            this.m_cbVersion.Name = "m_cbVersion";
            this.m_cbVersion.Size = new System.Drawing.Size(100, 21);
            this.m_cbVersion.TabIndex = 6;
            this.m_cbVersion.SelectedIndexChanged += new System.EventHandler(this.m_cbVersion_SelectedIndexChanged);
            // 
            // m_pbHisto
            // 
            this.m_pbHisto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_pbHisto.BackColor = System.Drawing.SystemColors.Window;
            this.m_pbHisto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbHisto.Location = new System.Drawing.Point(645, 130);
            this.m_pbHisto.Name = "m_pbHisto";
            this.m_pbHisto.Size = new System.Drawing.Size(330, 547);
            this.m_pbHisto.TabIndex = 8;
            this.m_pbHisto.TabStop = false;
            this.m_pbHisto.Click += new System.EventHandler(this.m_pbHisto_Click);
            // 
            // m_saveFileImage
            // 
            this.m_saveFileImage.Filter = "Image|*.jpg";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 16);
            this.label2.TabIndex = 10;
            this.label2.Text = "Analysis of :";
            // 
            // m_cbCase
            // 
            this.m_cbCase.FormattingEnabled = true;
            this.m_cbCase.Location = new System.Drawing.Point(133, 103);
            this.m_cbCase.Name = "m_cbCase";
            this.m_cbCase.Size = new System.Drawing.Size(118, 21);
            this.m_cbCase.TabIndex = 9;
            this.m_cbCase.SelectedIndexChanged += new System.EventHandler(this.m_cbCase_SelectedIndexChanged);
            // 
            // m_dgvStat
            // 
            this.m_dgvStat.AllowUserToAddRows = false;
            this.m_dgvStat.AllowUserToDeleteRows = false;
            this.m_dgvStat.AllowUserToResizeColumns = false;
            this.m_dgvStat.AllowUserToResizeRows = false;
            this.m_dgvStat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_dgvStat.BackgroundColor = System.Drawing.SystemColors.Window;
            this.m_dgvStat.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_dgvStat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.m_dgvStat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvStat.ColumnHeadersVisible = false;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.m_dgvStat.DefaultCellStyle = dataGridViewCellStyle5;
            this.m_dgvStat.Location = new System.Drawing.Point(451, 27);
            this.m_dgvStat.Name = "m_dgvStat";
            this.m_dgvStat.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.m_dgvStat.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.m_dgvStat.RowHeadersVisible = false;
            this.m_dgvStat.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.m_dgvStat.ShowCellErrors = false;
            this.m_dgvStat.ShowCellToolTips = false;
            this.m_dgvStat.ShowEditingIcon = false;
            this.m_dgvStat.ShowRowErrors = false;
            this.m_dgvStat.Size = new System.Drawing.Size(524, 85);
            this.m_dgvStat.TabIndex = 11;
            // 
            // m_btnWatch
            // 
            this.m_btnWatch.Enabled = false;
            this.m_btnWatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_btnWatch.Location = new System.Drawing.Point(13, 57);
            this.m_btnWatch.Name = "m_btnWatch";
            this.m_btnWatch.Size = new System.Drawing.Size(229, 24);
            this.m_btnWatch.TabIndex = 13;
            this.m_btnWatch.Text = "Show measurement data of ID ->";
            this.m_btnWatch.UseVisualStyleBackColor = true;
            this.m_btnWatch.Click += new System.EventHandler(this.m_btnWatch_Click);
            // 
            // m_tbID
            // 
            this.m_tbID.Location = new System.Drawing.Point(248, 60);
            this.m_tbID.Name = "m_tbID";
            this.m_tbID.Size = new System.Drawing.Size(91, 20);
            this.m_tbID.TabIndex = 14;
            // 
            // m_btnID
            // 
            this.m_btnID.Enabled = false;
            this.m_btnID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_btnID.Location = new System.Drawing.Point(136, 27);
            this.m_btnID.Name = "m_btnID";
            this.m_btnID.Size = new System.Drawing.Size(106, 24);
            this.m_btnID.TabIndex = 16;
            this.m_btnID.Text = "Filter w/ ID file";
            this.m_btnID.UseVisualStyleBackColor = true;
            this.m_btnID.Click += new System.EventHandler(this.m_btnID_Click);
            // 
            // m_openFileD3
            // 
            this.m_openFileD3.DefaultExt = "csv";
            this.m_openFileD3.FileName = "ID_List.csv";
            this.m_openFileD3.Filter = "CSV-Dateien|*.csv";
            this.m_openFileD3.Multiselect = true;
            // 
            // m_cbRepairs
            // 
            this.m_cbRepairs.AutoSize = true;
            this.m_cbRepairs.Location = new System.Drawing.Point(248, 32);
            this.m_cbRepairs.Name = "m_cbRepairs";
            this.m_cbRepairs.Size = new System.Drawing.Size(81, 17);
            this.m_cbRepairs.TabIndex = 22;
            this.m_cbRepairs.Text = "Only repairs";
            this.m_cbRepairs.UseVisualStyleBackColor = true;
            this.m_cbRepairs.CheckedChanged += new System.EventHandler(this.m_cbRepairs_CheckedChanged);
            // 
            // m_cbDup
            // 
            this.m_cbDup.AutoSize = true;
            this.m_cbDup.Location = new System.Drawing.Point(336, 32);
            this.m_cbDup.Name = "m_cbDup";
            this.m_cbDup.Size = new System.Drawing.Size(102, 17);
            this.m_cbDup.TabIndex = 23;
            this.m_cbDup.Text = "Allow duplicates";
            this.m_cbDup.UseVisualStyleBackColor = true;
            this.m_cbDup.CheckedChanged += new System.EventHandler(this.m_cbDup_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 688);
            this.Controls.Add(this.m_cbDup);
            this.Controls.Add(this.m_cbRepairs);
            this.Controls.Add(this.m_btnID);
            this.Controls.Add(this.m_tbID);
            this.Controls.Add(this.m_btnWatch);
            this.Controls.Add(this.m_dgvStat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_cbCase);
            this.Controls.Add(this.m_pbHisto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_cbVersion);
            this.Controls.Add(this.m_dgv);
            this.Controls.Add(this.m_lblParam);
            this.Controls.Add(this.m_cbParam);
            this.Controls.Add(this.m_btnFiles);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1000, 1000);
            this.MinimumSize = new System.Drawing.Size(1000, 726);
            this.Name = "Form1";
            this.Text = "Limmex Production Analysis Tool";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbHisto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvStat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog m_openFileD;
        private System.Windows.Forms.Button m_btnFiles;
        private System.Windows.Forms.ComboBox m_cbParam;
        private System.Windows.Forms.Label m_lblParam;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem selectParamterFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.DataGridView m_dgv;
        private System.Windows.Forms.ToolStripMenuItem selectLimitFileToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog m_openFileD2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox m_cbVersion;
        private System.Windows.Forms.PictureBox m_pbHisto;
        private System.Windows.Forms.SaveFileDialog m_saveFileImage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox m_cbCase;
        private System.Windows.Forms.DataGridView m_dgvStat;
        private System.Windows.Forms.Button m_btnWatch;
        private System.Windows.Forms.TextBox m_tbID;
        private System.Windows.Forms.Button m_btnID;
        private System.Windows.Forms.OpenFileDialog m_openFileD3;
        private System.Windows.Forms.CheckBox m_cbRepairs;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchWatchesByConditionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawGraphForGSMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showWatchIdsInFilteredDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yieldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perWeekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perMonthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatedFromLimitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDetailedAudioFailsToolStripMenuItem1;
        private System.Windows.Forms.CheckBox m_cbDup;
        private System.Windows.Forms.ToolStripMenuItem showAveragesOverTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    }
}

