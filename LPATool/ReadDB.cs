﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace LPATool
{
    class ReadDB
    {
        private const string HOSTNAME = "limmex-manufacturing.cylgcjpmtzkw.eu-west-1.rds.amazonaws.com";
        private const string DATABASENAME = "results_generic";
        private const string USERID = "dbuser";
        private const string PASSWD = "#%I\\hu46cCvNvfAUrbVh";
        private const string SQLCMD0 = "SELECT UUT_RESULT_DUMP FROM ";
        private const string SQLCMD1 = " where START_DATE_TIME > '";
        private const string SQLCMD2 = "' AND START_DATE_TIME < '"; 
        private const string SQLCMD3 = "' AND STATION_ID = '";
        private string m_connectionString;
        private  MySqlConnection m_connection;
        private  MySqlCommand m_command;
        private  MySqlDataReader m_reader;
        private Setup m_dbSetup;

        public ReadDB(Setup s)
        {
            m_dbSetup = s;
        }
        public bool pingDB()
        {
            bool flag = true;
            m_connectionString = "SERVER="   + m_dbSetup.HostName + ";" +
                                 "DATABASE=" + m_dbSetup.DatabaseName + ";" +
                                 "UID="      + m_dbSetup.User + ";" +
                                 "PASSWORD=" + m_dbSetup.Pwd + ";";
//            Debug.WriteLine("Connection string = " + m_connectionString);
            m_connection = new MySqlConnection(m_connectionString);
            m_command = m_connection.CreateCommand();
            try 
            {
                m_connection.Open();
                m_connection.Close();
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Exception = " + ex.ToString());
                flag = false; 
            }

            return flag;
        }
        public List<string> readData(DateTime dstart, DateTime dstop, string tester)
        {
            string dsa = dstart.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string dso = dstop.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            m_connection = new MySqlConnection(m_connectionString);   
            m_command = m_connection.CreateCommand();
            m_command.CommandText = buildSQL(dsa, dso, tester);    // build select statement
            m_connection.Open();
            try
            {
                m_reader = m_command.ExecuteReader();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error accessing DB: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            List<string> dataList = new List<string>();                 // stores the data sets
            dataList.Clear();
            while (m_reader.Read())
            {
                for (int i = 0; i < m_reader.FieldCount; i++)
                {
                    dataList.Add(m_reader.GetValue(i).ToString());
                }
            }
            m_connection.Close();
            return dataList;
        }
        private string buildSQL(string dsa, string dst, string tester)
        {
            string command = SQLCMD0 + m_dbSetup.DatabaseName + "." + m_dbSetup.TableName + SQLCMD1 + dsa + SQLCMD2 + dst;
            if(tester != "ALL")
            {
                command = command + SQLCMD3 + tester;
            }
            command += "';";
            Debug.WriteLine("SQL string = " + command);
            return command;
        }
    }
}
