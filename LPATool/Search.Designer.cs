﻿namespace LPATool
{
    partial class Search
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_lblParam = new System.Windows.Forms.Label();
            this.m_cbParam = new System.Windows.Forms.ComboBox();
            this.m_cbCompare = new System.Windows.Forms.ComboBox();
            this.m_tbVal = new System.Windows.Forms.TextBox();
            this.m_btnS = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_lblParam
            // 
            this.m_lblParam.AutoSize = true;
            this.m_lblParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblParam.Location = new System.Drawing.Point(11, 9);
            this.m_lblParam.Name = "m_lblParam";
            this.m_lblParam.Size = new System.Drawing.Size(111, 16);
            this.m_lblParam.TabIndex = 4;
            this.m_lblParam.Text = "Select parameter";
            // 
            // m_cbParam
            // 
            this.m_cbParam.FormattingEnabled = true;
            this.m_cbParam.Location = new System.Drawing.Point(12, 26);
            this.m_cbParam.Name = "m_cbParam";
            this.m_cbParam.Size = new System.Drawing.Size(144, 21);
            this.m_cbParam.TabIndex = 3;
            // 
            // m_cbCompare
            // 
            this.m_cbCompare.FormattingEnabled = true;
            this.m_cbCompare.Items.AddRange(new object[] {
            ">",
            ">=",
            "<",
            "<=",
            "== ",
            "!="});
            this.m_cbCompare.Location = new System.Drawing.Point(162, 26);
            this.m_cbCompare.Name = "m_cbCompare";
            this.m_cbCompare.Size = new System.Drawing.Size(127, 21);
            this.m_cbCompare.TabIndex = 5;
            // 
            // m_tbVal
            // 
            this.m_tbVal.Location = new System.Drawing.Point(295, 26);
            this.m_tbVal.Name = "m_tbVal";
            this.m_tbVal.Size = new System.Drawing.Size(115, 20);
            this.m_tbVal.TabIndex = 15;
            // 
            // m_btnS
            // 
            this.m_btnS.Location = new System.Drawing.Point(416, 23);
            this.m_btnS.Name = "m_btnS";
            this.m_btnS.Size = new System.Drawing.Size(71, 24);
            this.m_btnS.TabIndex = 16;
            this.m_btnS.Text = "Search";
            this.m_btnS.UseVisualStyleBackColor = true;
            this.m_btnS.Click += new System.EventHandler(this.m_btnS_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(161, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Comparison relation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(295, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "Value";
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 58);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_btnS);
            this.Controls.Add(this.m_tbVal);
            this.Controls.Add(this.m_cbCompare);
            this.Controls.Add(this.m_lblParam);
            this.Controls.Add(this.m_cbParam);
            this.Name = "Search";
            this.Text = "Search";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblParam;
        private System.Windows.Forms.ComboBox m_cbParam;
        private System.Windows.Forms.ComboBox m_cbCompare;
        private System.Windows.Forms.TextBox m_tbVal;
        private System.Windows.Forms.Button m_btnS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}